package TQMD;
# ABSTRACT: Subs for ToRQuEMaDA.

$TQMD::VERSION = '0.2.1';

use Modern::Perl '2011';
use autodie;

use Smart::Comments;

use Array::Split qw(split_by split_into);
use File::Basename;
use File::Find::Rule;
use List::Compare;
use List::AllUtils qw(uniq);
use Path::Class;

use Exporter::Easy (
    OK => [ qw(get_gcas_infos_dna get_gcas_infos_dna_custom get_gcas_infos_pep get_genomic get_list_compare get_new_gcas_db get_new_gcas_file get_protein split_into_array) ],
);


sub get_genomic {
    my $refseq_path = shift;
    my @dnafiles = File::Find::Rule
        ->file()
        ->name( '*_genomic.fna.gz' )
        ->not_name( '*_rna_from_genomic.fna.gz', '*_cds_from_genomic.fna.gz' )
        ->in($refseq_path)
    ;
    return \@dnafiles;
}

sub get_protein {
    my $refseq_path = shift;
    my @pepfiles = File::Find::Rule
        ->file()
        ->name( '*_protein.faa.gz' )
        ->in($refseq_path)
    ;
    return \@pepfiles;
}

sub get_gcas_infos_dna {
    my @dnafiles = @_;
    my @rsync_gcas;
    my %path_for;
    my %file_for;
    foreach my $path (@dnafiles){
        my ($filename, $dirs) = fileparse($path);
        my ($gcap1, $gcap2, undef) = split /_/xms, $filename;
        my $gca = join('_', $gcap1, $gcap2);
        push @rsync_gcas, $gca;
        $file_for{$gca} = $path;
        $path_for{$gca} = $dirs;
    }
    return(\@rsync_gcas,\%file_for,\%path_for);
}

sub get_gcas_infos_dna_custom {
    my @dnafiles = @_;
    my @rsync_gcas;
    my %path_for;
    my %file_for;
    my %fullname_for;
    foreach my $path (@dnafiles){
        my ($filename, $dirs) = fileparse($path);
        my ($gcap1, $gcap2, $genus, $species, undef) = split /_/xms, $filename;
        my $gca = join('_', $gcap1, $gcap2);
        my $fullname = join(' ', $genus, $species);
        push @rsync_gcas, $gca;
        $file_for{$gca} = $path;
        $path_for{$gca} = $dirs;
        $fullname_for{$gca} = $fullname;
    }
    return(\@rsync_gcas,\%file_for,\%path_for,\%fullname_for);
}

sub get_gcas_infos_pep {
    my @pepfiles = @_;
    my @rsync_gcas;
    my %path_for;
    my %file_for;
    foreach my $path (@pepfiles){
        my ($filename, $dirs) = fileparse($path);
        #~ if ($dirs =~ /latest_assembly_versions/) {
            my ($gcap1, $gcap2, undef) = split /_/xms, $filename;
            my $gca = join('_', $gcap1, $gcap2);
            push @rsync_gcas, $gca;
            $file_for{$gca} = $path;
            $path_for{$gca} = $dirs;
        #~ }
    }
    return(\@rsync_gcas,\%file_for,\%path_for);
}

sub get_new_gcas_db {
    my ($db_gcas, $rsync_gcas) = @_;
    my $lc = List::Compare->new('--unsorted', $db_gcas, $rsync_gcas);
    my @new_gcas = uniq( $lc->get_complement );
    return \@new_gcas;
}

sub get_new_gcas_file {
    my $gcalist = shift;
    my @new_gcas;
    my @taskids;
    open my $gcalistfile, '<', $gcalist;
    while (my $line = <$gcalistfile>) {
        chomp $line;
        push @new_gcas, $line;
    }
    push @taskids, split /_/, $gcalist;
    my $taskid = $taskids[-1];
    return(\@new_gcas, $taskid);
}

sub split_into_array {
    my ($pack, $new_gcas_ref, $type, $dir) = @_;
    my @sub_arrays = split_by($pack, @$new_gcas_ref);
    my $nbr_name = 1 ;
    my @nbr_array;

    foreach my $sub_array (@sub_arrays) {
        file($dir, "$type\_$nbr_name")->spew( join "\n", @{ $sub_array } );
        push @nbr_array, "$dir/$type\_$nbr_name";
        $nbr_name++;
    }
    my $limit = @nbr_array;
    file($dir, "$type\_array")->spew(join "\n", @nbr_array);
    return ($limit, \@nbr_array);
}

sub get_list_compare {
    my ($first, $second, $type) = @_;
    my @one = @{ $first };
    my @two = @{ $second };
    my $lc = List::Compare->new('--unsorted', \@one, \@two);
    my @list;
    if ($type eq 'intersection') {
        @list = $lc->get_intersection;
    } elsif ($type eq 'union') {
        @list = $lc->get_union;
    } elsif ($type eq 'unique') {
        @list = $lc->get_unique;
    } elsif ($type eq 'complement') {
        @list = $lc->get_complement;
    }
    return \@list;
}

1; # End of TQMD


=head1 AUTHORS

=over

=item Raphael R. LEONARD <rleonard@doct.uliege.be>

=item Denis BAURAIN <denis.baurain@uliege.be>

=back

=head1 LICENSE AND COPYRIGHT

This software is copyright (c) 2020 by University of Liege / Unit of Eukaryotic
Phylogenomics / Raphael R. LEONARD and Denis BAURAIN.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
