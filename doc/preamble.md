---
title: User Guide for TQMD
author: Raphaël R. LEONARD and Denis BAURAIN, ULiège
date: v0.2.1 - April 16th, 2022
header-includes:
  - \usepackage[sf,bf]{titlesec}

papersize: a4
geometry: margin=2.5cm
linestretch: 1.15

fontsize: 12pt
mainfont: EB Garamond
sansfont: Lato
monofont: InconsolataLGC

colorlinks: 1
linkcolor: black

---

<!--
$ pod2markdown -m lib/TQMD/Manual.pod doc/tqmd_manual.md
# previously this required very tiny tweaking in lists and internal links

$ pandoc --toc -N -s -t latex --template=doc/doc.latex --pdf-engine=xelatex \
    -o doc/tqmd_manual.pdf doc/preamble.md doc/tqmd_manual.md
-->
