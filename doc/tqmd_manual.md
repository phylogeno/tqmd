# NAME

ToRQuEMaDA: Tool for Retrieving Queried Eubacteria, Metadata and Dereplicating
Assemblies

# VERSION

version 0.2.1

# BACKGROUND

The fast-growing number of available prokaryotic genomes, along with their
uneven taxonomic distribution, is a problem when trying to assemble broadly
sampled genome sets for phylogenomics and comparative genomics. Indeed, most of
the new genomes belong to the same subset of hyper-sampled phyla while the
continuous flow of newly discovered phyla prompts for regular updates of local
databases. This situation makes it difficult to maintain sets of representative
genomes combining lesser known phyla, for which few species are available, and
sound subsets of highly abundant phyla.

An automated straightforward method is required but would be far too slow if
based on regular alignment algorithms. The objective of **ToRQuEMaDA** ("Tool for
Retrieving Queried Eubacteria, Metadata and Dereplicating Assemblies" or **TQMD**
for short) is to provide the user with a list of dereplicated genomes
representative of the prokaryotic diversity (or of a user-defined subset of it).
Its output takes the form of a single list of NCBI GCA/GCF numbers, based on a
distance metric derived from the proportion of shared _k_-mers between two
pairs of genomes. Such a GCA/GCF number is the unique identifier of a particular
genome assembly (see
[https://www.ncbi.nlm.nih.gov/datasets/docs/gca-and-gcf-explained/](https://www.ncbi.nlm.nih.gov/datasets/docs/gca-and-gcf-explained/) for
details).

**TQMD** is composed of five scripts combining **Perl** and **MySQL 5**. The first
script (`tqmd_download.pl`) downloads bacterial and archaeal genomes from
_NCBI RefSeq_ or _GenBank_, stores them locally and indexes their metadata in
a database. The second script (`tqmd_update.pl`) launches several sub-scripts
that compute the different pieces of information needed by **TQMD** to
dereplicate and select the best representative genomes. These sub-scripts, once
executed, also store the computed data in the database. They have to be launched
before the third script to allow **TQMD** to gather enough information to perform
its duties: _k_-mer composition of the genomes, quality of the genome
assemblies, richness of the genome annotations, SSU rRNA (16S) sequence
prediction and subsequent taxonomic analysis, contamination level of the genome
assemblies. The third script (`tqmd_cluster.pl`) is the main script, ensuring
dereplication and selection of the representative genomes. The fourth script
(`tqmd_import.pl`) is a script allowing the user to add and process custom
genomes not present in _RefSeq_ or _GenBank_. The fifth script
(`tqmd_debrief.pl`) is designed to analyze the results of the dereplication
process, producing a file that shows the genomes clustered behind each
representative.

`tqmd_download.pl` and `tqmd_update.pl` are meant to be used periodically
(once every month or two months) to keep the local genome database in sync with
_NCBI_ servers. Once the first two scripts have been run at least once,
`tqmd_cluster.pl` can be launched whenever needed to obtain representative
genomes. In contrast, the two last scripts are completely optional:
`tqmd_import.pl` can be used as soon as the first script (`tqmd_download.pl`)
has setup the local database, whereas `tqmd_debrief.pl` can be run at will to
parse the clustering output files and get insight into the process.

# INSTALLATION

Before being able to launch **TQMD**, you need several programs and dependencies
to be installed on your computer, including a **MySQL 5** database server.

There exists a script (`.def`) for building a **Singularity** container that
provides a complete install of **TQMD** for single-node computing servers (see
the online
[README.md](https://bitbucket.org/phylogeno/tqmd/src/develop/README.md) for the
most up-to-date details). This file further includes commmands to process a
small test case (optionally using **Forty-Two**). Below are the instructions for
a full custom install of **TQMD** on a computing cluster.

First, **SGE** (Sun/Open Grid Engine) has to be installed, since **TQMD** is based
on it to function properly. Then seven different programs used by
`tqmd_update.pl` and/or `tqmd_cluster.pl` have to be installed: **JELLYFISH**,
**Mash**, **QUAST**, **RNAmmer**, **CD-HIT**, **CheckM** and **Forty-Two**.

What follows is the list of specific Perl dependencies. If some are not yet
available on your system, they can be installed using, e.g., `cpanm`.

- [Array::Split](https://metacpan.org/pod/Array%3A%3ASplit)
- [Array::Utils](https://metacpan.org/pod/Array%3A%3AUtils)
- [Config::IniFiles](https://metacpan.org/pod/Config%3A%3AIniFiles)
- [Data::UUID](https://metacpan.org/pod/Data%3A%3AUUID)
- [DBI](https://metacpan.org/pod/DBI)
- [DBI::DBD](https://metacpan.org/pod/DBI%3A%3ADBD)
- [DBD::mysql](https://metacpan.org/pod/DBD%3A%3Amysql)
- [Statistics::Data::Rank](https://metacpan.org/pod/Statistics%3A%3AData%3A%3ARank)

Second, **TQMD** makes use of the taxonomic methods provided by
[Bio::MUST::Core](https://metacpan.org/pod/Bio%3A%3AMUST%3A%3ACore), also available on CPAN. The last step before installing
**TQMD** is to configure a MySQL 5 database using the **TQMD** SQL schema (the
`.sql` file is provided). Once the database is installed, you need to create
the first entries manually:

        INSERT INTO Users Values ("admin", "<password>", "<firstname name>",
                "<affiliated group>", "<email adress>", NOW());
        INSERT INTO Groups VALUES ("public","public group",NOW());
        INSERT INTO Memberships VALUES ("admin","public");

Third, you need to modify the script templates coded within `tqmd_update.pl`
and `tqmd_cluster.pl` in order to modify the `-q` option to suit your own SGE
installation:

        #\$ -q <queuename>

Fourth, you need to create an `.ini` file with the following informations:

        [ Database ]
        database=DBI:mysql:tqmd;host=XXX.XXX.XXX.XXX
        username=tqmd
        password=<password>
        [ Dependencies ]
        jellyfish_exe=<path or name of JELLYFISH exec>
        mash_exe=<path or name of Mash exec>
        quast_exe=<path or name of QUAST exec>
        rnammer_exe=<path or name of RNAmmer exec>
        cdhitest_exe=<path or name of CD-HIT-EST exec>
        checkm_exe=<path or name of CheckM exec>
        makeblastdb_exe=<path or name of makeblastdb exec>

That `.ini` file was created to avoid hard-coding the `Database` connection
details in the scripts. This file is expected to be expanded with more
configuration keys in later versions of **TQMD**. The `Dependencies` part of the
`.ini` file was created to allow the user to specify which installation s/he
wishes to use without requiring to modify **TQMD** code.

# USAGE

As mentioned above, the scripts have to be launched in a specific order for the
first use of **TQMD**. This manual follows this order.

## tqmd\_download.pl

        $ tqmd_download.pl --tqmd-dir=<dir> --coll-name=<str> --config=<file> \
              [--source=<str>] [--filter=<file>] [--setup-taxdir]

- `--tqmd-dir=<dir`> Full path to the place you want **TQMD** to store the
genomes and proteomes (e.g., `/home/leonard/work/TQMD_DIR/`).
- `--coll-name=<str`> Name of the update which will be stored within the
database. We advise you to use the date in the following format for the name
`YYYY_MM_DD_source`. The source is described below.
- `--config=<file`> Path to the INI file specifying TQMD configuration. If
this file is not located in the same directory used to launch **TQMD**, add the
full path before the name (e.g.,
`/home/leonard/work/TQMD_DIR/tqmd_config.ini`).
- `--source=<str`> Source database from where to download genomes and
proteomes (either NCBI GenBank or NCBI RefSeq, respectively designated as
`genbank` and `refseq`) \[default: `refseq`\].
- `--filter=<file`> Path to an IDL file specifying the taxonomic filter to
be applied when downloading genomes \[default: `+Archaea, +Bacteria`\].

    In a tax\_filter, wanted taxa are to be prefixed by a `+` symbol, whereas
    unwanted taxa are to be prefixed by a `-` symbol. Wanted taxa are linked by
    logical ORs while unwanted taxa are linked by logical ANDs. Unwanted taxa should
    not include wanted taxa because the former ones take precedence over the latter
    ones. In contrast the opposite helps fine-tuning the tax\_filter.

    An example IDL file follows:

        +Archaea
        +Bacteria
        -Proteobacteria
        -Firmicutes

- `--setup-taxdir` Force update the local mirror of the NCBI Taxonomy
database.

As mentioned earlier, for the first use of **TQMD** you have to begin with this
script. Then you can use it periodically; we advise you to run it once every
month or two months.

## tqmd\_update.pl

    $ tqmd_update.pl --tqmd-dir=<dir> --temp-dir=<dir> --calc=<str> \
          --config=<file> [--config-42=<file>] \
          [--kmer-size=<n>] [--kmer-canonical] [--cdhit-threshold=<n>] \
          [--pack-size=<n>] [--max-array=<n>] [--threads=<n>] [--single-node]

- `--tqmd-dir=<dir`> Full path to the place you want **TQMD** to store the
genomes and proteomes (e.g., `/home/leonard/work/TQMD_DIR/`).
- `--temp-dir=<dir`> Full path to the place which will be used to store the
temporary files required by the sub-scripts to produce the data.
- `--calc=<str`> Type of calculation to be performed. This must be one
string out of `quast`, `annotation`, `jellyfish`, `rnammer`, `fortytwo` and
`checkm`.
- `--config=<file`> Path to the INI file specifying TQMD configuration. If
this file is not located in the same directory used to launch **TQMD**, add the
full path before the name (e.g.,
`/home/leonard/work/TQMD_DIR/tqmd_config.ini`).
- `--config-42=<file`> Path to the INI file specifying FortyTwo
configuration. If this file is not located in the same directory used to launch
**TQMD**, add the full path before the name (e.g.,
`/home/leonard/work/TQMD_DIR/tqmd_config_FT.ini`).
- `--kmer-size=<n`> Size of the **JELLYFISH** kmer \[default: `12`\].
- `--kmer-canonical` When specified, canonical kmers are used (instead of
strand-specific kmers).
- `--cdhit-threshold=<n`> **CD-HIT** threshold determining when to cluster
SSU rRNAs (16S) \[default: `0.975`\].
- `--pack-size=<n`> Maximum number of genomes by sub-job of a job-array
\[default: `200`\].
- `--max-array=<n`> Maximum number of sub-jobs/CPUs to run/use in parallel
\[default: `100`\].
- `--threads=<n`> Number of threads/CPUs to be used by **JELLYFISH**,
**QUAST** and **CheckM** \[default: `1`\].
- `--single-node` When specified, **TQMD** works on a single node (instead
of the grid engine).

When the `--kmer-engine` of `tqmd_cluster.pl` is `jellyfish`,
`tqmd_update.pl` must be run with the option `--calc=jellyfish` after each run
of `tqmd_download.pl` and/or `tqmd_import.pl`. This is not required when the
envisioned `--kmer-engine` is `mash`. In any case, at least one metric must be
selected in the `--ranking-formula` of `tqmd_cluster.pl`. Moreover, all the
selected metrics must have been computed using `tqmd_update.pl --calc=...`
before launching `tqmd_cluster.pl`.

## tqmd\_cluster.pl

    $ tqmd_cluster.pl --tqmd-dir=<dir> --temp-dir=<dir> --config=<file> \
          [--source=<str>] [--filter=<file>] \
          [--gca-list=<file>] [--negative-gca-list=<file>] \
          [--custom-genomes] [--requires-SSU-rRNA] [--collections=<str>] \
          [--dist-metric=<str>] [--dist-threshold=<n>] \
          [--clustering-mode=<str>] [--dividing-scheme=<str>] \
          [--ranking-formula=<formula>] [--priority-gca-list=<file>] \
          [--kmer-engine=<str>] [--kmer-size=<n>] [--kmer-canonical] \
          [--repr-threshold=<n_l>x<n_p>] [--delta-limit=<n>] \
          [--min-round=<n>] [--max-round=<n>] \
          [--pack-size=<n>] [--max-array=<n>] [--threads=<n>] [--single-node]

- `--tqmd-dir=<dir`> Full path to the place you want **TQMD** to store the
genomes and proteomes (e.g., `/home/leonard/work/TQMD_DIR/`).
- `--temp-dir=<dir`> Full path to the place which will be used to store the
temporary files required by the sub-scripts to produce the data.
- `--config=<file`> Path to the INI file specifying TQMD configuration. If
this file is not located in the same directory used to launch **TQMD**, add the
full path before the name (e.g.,
`/home/leonard/work/TQMD_DIR/tqmd_config.ini`).
- `--source=<str`> Source database to use for clustering (either NCBI
GenBank or NCBI RefSeq, respectively designated as `genbank` and `refseq`)
\[default: `refseq`\].
- `--filter=<file`> Path to an IDL file specifying the taxonomic filter to
be applied when clustering genomes \[default: none\].

    Note that this tax\_filter can only remove genomes. It cannot include genomes
    that have not yet been introduced in the database using the other **TQMD**
    scripts. See `tqmd_download.pl` for the exact syntax of such a filter.

- `--gca-list=<file`> Path to a file containing a subset of GCA/GCF numbers
you wish to work with \[default: none\]. If this option is not used, **TQMD** uses
every genome available. If the file is not located in the same directory you are
using to launch **TQMD**, add the full path before the name.
- `--negative-gca-list=<file`> Path to a file containing a list of every
GCA/GCF numbers you do NOT want to use \[default: none\]. If the file is not
located in the same directory you are using to launch **TQMD**, add the full path
before the name.
- `--custom-genomes` When specified, **TQMD** will also take custom genomes
into consideration \[default: no\].
- `--requires-SSU-rRNA` When specified, **TQMD** will only take into
consideration the genomes where at least one SSU rRNA (16S) sequence has been
predicted by the rnammer sub-script of `tqmd_update.pl` \[default: no\].
- `--collections=<str`> Name of one or more collection(s) you want to use.
Collection names correspond to the `--coll-name` option of `tqmd_download.pl`.
Names have to be provided between single quotes and, if several collection names
are given, they must be separated by a comma (`,`).
- `--dist-metric=<str`> Distance metric to use for the clustering (either
Jaccard Index (`JI`) or Identical Genome Fraction (`IGF`) \[default: `JI`\].
Note that these distances are conceptually closer to similarity metrics (i.e.,
the larger the metric value the more similar the two genomes). Thus, the
distance effectively used by **TQMD** is obtained either as 1-JI or 1-IGF.
- `--dist-threshold=<n`> Minimum threshold above which to cluster genomes
together \[default: `0.6`\]. The larger the number, the more aggressive **TQMD**
becomes.
- `--clustering-mode=<str`> Heuristic to use when clustering genomes
(either `strict` or `loose`) \[default: `loose`\]. In `loose` mode, each
genome is compared against every other genome until finding one that is close
enough, whereas in `strict` mode, genomes are only compared to the
representatives of each cluster. The `strict` mode is faster and less
agglomerative than the `loose` mode.
- `--dividing-scheme=<str`> Type of partitioning **TQMD** uses to allocate
genomes to sub-jobs (either `taxonomic` or `random`) \[default: `taxonomic`\].
If
`random` is chosen, `--max-round` should be increased from 10 to 20-30.
- `--kmer-engine=<str`> Kmer counting program to use to compute distance
metrics (either `jellyfish` or `mash`) \[default: `jellyfish`\]. If `mash` is
chosen, `--dist-metric` must be set to `JI` and `--kmer-size` should be
increased from 12 to 14-16.
- `--kmer-size=<n`> Size of the **JELLYFISH** or **Mash** kmer used for
pairwise genome comparisons \[default: `12`\]. When `--kmer-engine` is set to
`jellyfish`, this option further fetches the corresponding single-genome values
stored in the database. In contrast, `mash` computations always happen live.
- `--kmer-canonical` When specified, canonical kmers are used (instead of
strand-specific kmers).
- `--ranking-formula=<formula`> Assembly-related metrics to be taken into
account when ranking genomes to select the cluster representatives. Each metric
can be considered either in ascending (`+`) or decending (`-`) order. For
example, a contamination metric should be prefixed by `-` (the lower the
better) whereas a completeness metric should be prefixed by `+` (the higher the
better).

    By default, **TQMD** ranking is computed as:

        -quast.N.per.100.kbp                # assembly quality
        +quast.largest.contig.ratio         # assembly quality
        +annot.certainty                    # annotation richness
        -42.contam.perc                     # contamination level
        +42.added.ali                       # completeness level

    All available metrics are:

        quast.contigs.0.bp
        quast.contigs.1000.bp
        quast.total.len.0.bp
        quast.total.len.1000.bp
        quast.contigs
        quast.largest.contig
        quast.total.len
        quast.gc.perc
        quast.n50
        quast.n75
        quast.l50
        quast.l75
        quast.N.per.100.kbp
        quast.largest.contig.ratio
        annot.certainty
        annot.completeness
        42.added.ali
        42.clean.ali
        42.contam.ali
        42.added.seq
        42.clean.seq
        42.contam.seq
        42.foreign.phyla
        42.contam.perc
        42.class.contam.perc
        42.unclass.contam.perc
        42.unknown.perc
        cm.completeness
        cm.contamination
        cm.strain.heterogeneity

- `--priority-gca-list=<file`> Path to a file containing a subset of
GCA/GCF numbers you wish to give priority in the ranking \[default: none\]. If the
file is not located in the same directory you are using to launch **TQMD**, add
the full path before the name.
- `--repr-threshold=<n_l`x&lt;n\_p>> Target number and tolerance value (in
percent) for the number of desired representative genomes \[default: `200x25`\].
The default value translates to "two hundred genomes approximated to 25%, i.e.,
maximum 250 genomes". This is the first of the three stop conditions controlling
the iterations of **TQMD**.
- `--delta-limit=<n`> Minimum level of dereplication (in percent) to
achieve between two successive iterations to continue \[default: `1`\]. This is
the second of the three stop conditions controlling the iterations of **TQMD**.
- `--pack-size=<n`> Maximum number of genomes by sub-job of a job-array
\[default: `200`\].
- `--max-array=<n`> Maximum number of sub-jobs/CPUs to run/use in parallel
\[default: `100`\].
- `--threads=<n`> Number of threads/CPUs to be used by **JELLYFISH**
\[default: `1`\].
- `--single-node` When specified, **TQMD** works on a single node (instead
of the grid engine).
- `--max-round=<n`> Maximum number of iterative rounds allowed \[default:
`10`\]. There is generally no reason to change this value except when setting
the option `--dividing-scheme` to `random`. This is the third of the three
stop conditions controlling the iterations of **TQMD**.
- `--min-round=<n`> Minimum number of rounds required \[default `1`\].

## tqmd\_import.pl

    $ tqmd_import.pl --tqmd-dir=<dir> --coll-name=<str> --config=<file>

- `--tqmd-dir=<dir`> Full path to the place you want **TQMD** to store the
genomes and proteomes (e.g., `/home/leonard/work/TQMD_DIR/`).
- `--coll-name=<str`> Name of the update which will be stored within the
database. We advise you to use the date in the following format for the name
`YYYY_MM_DD`.
- `--config=<file`> Path to the INI file specifying TQMD configuration. If
this file is not located in the same directory used to launch **TQMD**, add the
full path before the name (e.g.,
`/home/leonard/work/TQMD_DIR/tqmd_config.ini`).

The genomes and proteomes the user wishes to import must be placed in a
directory called `custom` manually created within the directory `archives`
which is itself within the directory specified by the user with the
`--tqmd-dir` option (e.g., `/home/leonard/word/TQMD_DIR/archives/custom/`).
The genomes are mandatory but not the proteomes.

The genomes and proteomes have to be manually renamed and compressed:

    XXX_YYYYYYYY_Genus_species_genomic.fna.gz
    XXX_YYYYYYYY_Genus_species_protein.faa.gz

`XXX_YYYYYYY` is a unique identifier to be created by the user. The total
length cannot be over 45 characters and the first three characters `XXX` cannot
be `GCA` or `GCF`. The underscores are mandatory. The correct importation of
the genome depends on the name used and is the responsibility of the user.
Deviation of the nomenclature is likely to result in failure.

## tqmd\_debrief.pl

    $ tqmd_debrief.pl --tqmd-dir=<dir> --temp-dir=<dir>

- `--tqmd-dir=<dir`> Full path to the place you want **TQMD** to store the
genomes and proteomes (e.g., `/home/leonard/work/TQMD_DIR/`).
- `--temp-dir=<dir`> Full path to the place which will be used to store the
temporary files required by the sub-scripts to produce the data.

# AUTHORS

- Raphael R. LEONARD <rleonard@doct.uliege.be>
- Denis BAURAIN <denis.baurain@uliege.be>

# LICENSE AND COPYRIGHT

This software is copyright (c) 2020 by University of Liege / Unit of Eukaryotic
Phylogenomics / Raphael R. LEONARD and Denis BAURAIN.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
