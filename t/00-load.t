#!perl
use 5.012;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'TQMD' ) || print "Bail out!\n";
}

diag( "Testing TQMD $TQMD::VERSION, Perl $], $^X" );
