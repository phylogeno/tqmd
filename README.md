# ToRQuEMaDA

version 0.2.1 - 16 April 2022


## Overview

__ToRQuEMaDA__ (__TQMD__ for short) is a tool for high-performance computing
clusters (and powerful single-node servers) which downloads, stores and produces
lists of dereplicated prokaryotic genomes. It has been developed to counter the
ever-growing number of prokaryotic genomes and their uneven taxonomic
distribution. It is based on word-based alignment-free methods (*k*-mers), an
iterative single-linkage approach and a divide-and-conquer strategy to remain
both efficient and scalable. __TQMD__ is written in __Modern Perl__.

### Installation options

__TQMD__ has a number of external dependencies and relies on a __MySQL__
database. The file `tqmd_manual.pdf` available in the `doc` directory of this
distribution details the installation for a __SGE/OGE__-based computing cluster.
If you want to use __TQMD__ on a single-node server (including a node of a
cluster running another scheduler), we advise you to use the provided script to
build a __Singularity__ container including __TQMD__ and all its dependencies.
Below are the instructions to do this and to test the resulting __TQMD__
installation using a small example dataset of about 300 cyanobacterial genomes.

### Documentation

Beyond the User Guide (`doc/tqmd_manual.pdf`), after installing, you can find
documentation for this software with the `perldoc` command.

    $ perldoc TQMD::Manual


## TQMD for Singularity

### Singularity install

Follow the online instructions to install __Singularity__ 3 (or above)
<https://sylabs.io/guides/3.7/admin-guide/admin_quickstart.html>.

### TQMD container build

Download the `TQMD.def` file from our __Bitbucket__ repository.

    $ wget https://bitbucket.org/phylogeno/tqmd/downloads/TQMD-no-hmmer.def

Build the container.

    $ sudo singularity build TQMD.sif TQMD-no-hmmer.def

### MySQL container setup

The following instructions come from
<https://github.com/pescobar/mysql-singularity-container>.

    $ mkdir -p ${PWD}/mysql/var/lib/mysql ${PWD}/mysql/run/mysqld
    $ curl https://raw.githubusercontent.com/ISU-HPC/mysql/master/my.cnf > ${HOME}/.my.cnf
    $ curl https://raw.githubusercontent.com/ISU-HPC/mysql/master/mysqlrootpw > ${HOME}/.mysqlrootpw
    $ singularity instance start --bind ${PWD}/mysql/var/lib/mysql/:/var/lib/mysql \
        --bind ${PWD}/mysql/run/mysqld:/run/mysqld shub://ISU-HPC/mysql mysql
    $ singularity run instance://mysql

In `.mysqlrootpw`, the password for __MySQL__ is set up as `my-secret-pw` and
the user as `root`. You can change that but then you will need to adapt the
relevant parts of the present protocol.  

### TQMD database setup

You will need the `tqmd.sql` and `init.sql` files that are in the `sql` dir of
the __TQMD__ distribution. The first contains the __MySQL__ schema of the
relational database, whereas the second creates a default user for the database.

Enter into the Singularity MySQL instance.

    $ singularity shell instance://mysql

Create and setup the database.

    $ mysql --user=root --password=my-secret-pw < tqmd.sql
    $ mysql --user=root --password=my-secret-pw --database=tqmd < init.sql

Log off the Singularity MySQL instance by pressing `Ctrl-D`.


### TQMD tests

#### Configuration files

Create the configuration file (`tqmd_config.ini`) as shown below. This file
provides the database connection credentials and the paths to the external
dependencies. Under __Singularity__, these credentials are very simple and all
the binaries should be available in the `$PATH`.

    $ cat tqmd_config.ini
    [ Database ]
    database=DBI:mysql:tqmd
    username=root
    password=my-secret-pw
    [ Dependencies ]
    jellyfish_exe=jellyfish
    mash_exe=mash
    quast_exe=quast-lg.py
    rnammer_exe=rnammer
    cdhitest_exe=cd-hit-est
    checkm_exe=checkm
    makeblastdb_exe=makeblastdb

Create a taxonomic filter file (`nostoc.idl`). This file uses `Bio::MUST::Core`
syntax and tells __TQMD__ to only consider genomes belonging to *Nostocales*.

    $ cat nostoc.idl
    +Nostocales

#### Workflow overview

The minimal workflow for running __TQDM__ is as follows:

1. `tqmd_download.pl ...`
2. `tqmd_update.pl --calc=...`
3. `tqmd_cluster.pl ...`
4. `tqmd_debrief.pl ...`

`tqmd_download.pl` downloads the genomes from the NCBI servers. `tqmd_update.pl`
populates the database with precomputed *k*-mers (if using __JELLYFISH__) and
quality metrics. It must be launched once for computing the *k*-mers and once
for each metric considered during the clustering.

In the following, we provide the instructions for generating both __QUAST__ and
__Forty-Two__-related quality metrics. Since the latter requires more setup, you
might want to bypass it for a quick test. This is possible but then be sure to
use the alternative call to `tqmd_cluster.pl` provided below.

The commands are written so that common options are always at the same location
on the command line. This should help you to understand when they are required
and what they do. Those that are mandatory are related to directory binding for
__Singularity__ and provide the paths to __TQMD__ working dir and config file.

    # common options (do not type them now)
    --bind ${PWD}/mysql/var/lib/mysql/:/var/lib/mysql
    --bind ${PWD}/mysql/run/mysqld:/run/mysqld
    --bind ${PWD}:/mnt TQMD.sif tqmd_download.pl
    --tqmd-dir=/mnt/nostoc_test/ --config=/mnt/tqmd_config.ini

#### Genome download -- `tqmd_download.pl`

Run `tqmd_download.pl` for downloading genomes.

    $ singularity exec \
        --bind ${PWD}/mysql/var/lib/mysql/:/var/lib/mysql \
        --bind ${PWD}/mysql/run/mysqld:/run/mysqld \
        --bind ${PWD}:/mnt TQMD.sif tqmd_download.pl \
        --tqmd-dir=/mnt/nostoc_test/ --config=/mnt/tqmd_config.ini \
        --source=refseq --coll-name=20210301_refseq \
        --filter=/mnt/nostoc.idl --setup-taxdir

#### Metric computation -- `tqmd_update.pl`

Run `tqmd_update.pl` for computing __JELLYFISH__ *k*-mers.

    $ singularity exec \
        --bind ${PWD}/mysql/var/lib/mysql/:/var/lib/mysql \
        --bind ${PWD}/mysql/run/mysqld:/run/mysqld \
        --bind ${PWD}:/mnt TQMD.sif tqmd_update.pl \
        --tqmd-dir=/mnt/nostoc_test/ --config=/mnt/tqmd_config.ini \
        --temp-dir=prep_jellyfish \
        --calc=jellyfish \
        --max-array=10 --single-node 2> tqmd_prep_jellyfish

Run `tqmd_update.pl` for computing __QUAST__-related quality metrics.

    $ singularity exec \
        --bind ${PWD}/mysql/var/lib/mysql/:/var/lib/mysql \
        --bind ${PWD}/mysql/run/mysqld:/run/mysqld \
        --bind ${PWD}:/mnt TQMD.sif tqmd_update.pl \
        --tqmd-dir=/mnt/nostoc_test/ --config=/mnt/tqmd_config.ini \
        --temp-dir=prep_quast \
        --calc=quast \
        --max-array=10 --single-node 2> tqmd_prep_quast

Run `tqmd_update.pl` for computing annotation-related quality metrics. This
step does not requires any external dependency. However, it only works for
genomes having a predicted proteome (automatically downloaded if any).

    $ singularity exec \
        --bind ${PWD}/mysql/var/lib/mysql/:/var/lib/mysql \
        --bind ${PWD}/mysql/run/mysqld:/run/mysqld \
        --bind ${PWD}:/mnt TQMD.sif tqmd_update.pl \
        --tqmd-dir=/mnt/nostoc_test/ --config=/mnt/tqmd_config.ini \
        --temp-dir=prep_annotation \
        --calc=annotation \
        --max-array=10 --single-node 2> tqmd_prep_annotation

#### Optional step: __Forty-Two__ run -- `tqmd_update.pl`

If you want to use __Forty-Two__-related quality metrics when clustering, you
need to setup it as detailed below. Otherwise, skip directly to the alternative
form of `tqmd_cluster.pl`
[below](#genome-clustering-without-forty-two-tqmd_cluster.pl).

Clone the repository with all the reference data files.

    $ git clone https://bitbucket.org/phylogeno/42-ribo-msas

The repository includes files for both prokaryotic and eukaryotic ribosomal
proteins. However, __TQMD__ currently only assesses prokaryotic genomes.

Important prebuilt files are the following:

- `42-ribo-msas/queries/queries-prokaryotes.idl`
- `42-ribo-msas/labelers/seq-labels.idl`
- `42-ribo-msas/labelers/contam-labels.idl`

Build BLAST databases for the proteomes of reference organisms.

    $ cd 42-ribo-msas/ref_orgs/prokaryotes/
    $ for REFORG in *.fasta; do makeblastdb -in $REFORG -dbtype prot \
        -out `basename $REFORG .fasta` -parse_seqids; done
        
Create configuration file for __Forty-Two__.

    $ cd ../../../
    $ cat fortytwo_config.ini
    [ Config ]
    ribo_dir=/mnt/42-ribo-msas/MSAs/prokaryotes
    ref_bank_dir=/mnt/42-ribo-msas/ref_orgs/prokaryotes/
    queries=/mnt/42-ribo-msas/queries/queries-prokaryotes.idl
    seq_labeling=/mnt/42-ribo-msas/labelers/seq-labels.idl
    contam_labeling=/mnt/42-ribo-msas/labelers/contam-labels.idl
    ref_org_mul=0.30

Run `tqmd_update.pl` for computing __Forty-Two__-related quality metrics. As
explained above, this step is optional. If you decide to skip it, use the second
form of the `tqmd_cluster.pl` command
[below](#genome-clustering-without-forty-two-tqmd_cluster.pl).

    $ singularity exec \
        --bind ${PWD}/mysql/var/lib/mysql/:/var/lib/mysql \
        --bind ${PWD}/mysql/run/mysqld:/run/mysqld \
        --bind ${PWD}:/mnt TQMD.sif tqmd_update.pl \
        --tqmd-dir=/mnt/nostoc_test/ --config=/mnt/tqmd_config.ini \
        --temp-dir=prep_fortytwo --config-42=/mnt/fortytwo_config.ini \
        --calc=fortytwo \
        --pack-size=500 --max-array=10 --single-node 2> tqmd_prep_fortytwo

#### Genome clustering (with __Forty-Two__) -- `tqmd_cluster.pl`

Run `tqmd_cluster.pl` using default ranking formula.

    $ singularity exec \
        --bind ${PWD}/mysql/var/lib/mysql/:/var/lib/mysql \
        --bind ${PWD}/mysql/run/mysqld:/run/mysqld \
        --bind ${PWD}:/mnt TQMD.sif tqmd_cluster.pl \
        --tqmd-dir=/mnt/nostoc_test/ --config=/mnt/tqmd_config.ini \
        --temp-dir=nostoc_refseq --filter=/mnt/nostoc.idl \
        --dist-metric=JI --dist-threshold=0.70 --kmer-size=12 \
        --clustering-mode=strict --dividing-scheme=taxonomic \
        --min-round=1 --max-round=10 \
        --max-array=10 --single-node 2> nostoc_refseq

#### Genome clustering (without __Forty-Two__) -- `tqmd_cluster.pl`

Run `tqmd_cluster.pl` using custom ranking formula.

    $ singularity exec \
        --bind ${PWD}/mysql/var/lib/mysql/:/var/lib/mysql \
        --bind ${PWD}/mysql/run/mysqld:/run/mysqld \
        --bind ${PWD}:/mnt TQMD.sif tqmd_cluster.pl \
        --tqmd-dir=/mnt/nostoc_test/ --config=/mnt/tqmd_config.ini \
        --temp-dir=nostoc_refseq --filter=/mnt/nostoc.idl \
        --dist-metric=JI --dist-threshold=0.70 --kmer-size=12 \
        --clustering-mode=strict --dividing-scheme=taxonomic \
        --ranking-formula='-quast.N.per.100.kbp, +quast.largest.contig.ratio, +annot.certainty' \
        --min-round=1 --max-round=10 \
        --max-array=10 --single-node 2> nostoc_refseq

#### Cluster analysis -- `tqmd_debrief.pl`

The output of `tqmd_cluster.pl` includes a `*.result` file listing the GCA/GCF
numbers of the representative genomes. With the example dataset (312 genomes as
of March 2021), the clustering yields 34 representatives.

    $ wc -l p200_l200_t0.70_j12_a6_r10_d1_min1_typetaxonomic_algJI_egnjellyfish.result
    34 ...
    
    $ head -n5 p200_l200_t0.70_j12_a6_r10_d1_min1_typetaxonomic_algJI_egnjellyfish.result
    GCF_009498015.1
    GCF_000316625.1
    GCF_000316575.1
    GCF_003054475.1
    GCF_001277295.1

To get the cluster members behind the representatives, run `tqdm_debrief.pl`.

    $ singularity exec \
        --bind ${PWD}/mysql/var/lib/mysql/:/var/lib/mysql \
        --bind ${PWD}/mysql/run/mysqld:/run/mysqld \
        --bind ${PWD}:/mnt TQMD.sif tqmd_debrief.pl \
        --tqmd-dir=/mnt/nostoc_test/ \
        --temp-dir=nostoc_refseq

The output of this script is a `*.debrief` file that has a form similar to the
result files of orthology inference pipelines.

    $ less p200_l200_t0.70_j12_a6_r10_d1_min1_typetaxonomic_algJI_egnjellyfish.debrief
    ...
    GCF_000316575.1:GCF_000316575.1
    GCF_003054475.1:GCF_000169135.1,GCF_003054475.1,GCF_001623485.1,...
    GCF_001277295.1:GCF_014696815.1,GCF_009711925.1,GCF_015207875.1,...
    ...


## References

- Léonard et al. (2021) ToRQuEMaDA: Tool for Retrieving Queried Eubacteria,
Metadata and Dereplicating Assemblies (in revision for PeerJ).


## License and Copyright

This software is copyright (c) 2020 by University of Liege / Unit of Eukaryotic
Phylogenomics / Raphael R. LEONARD and Denis BAURAIN.

This is free software; you can redistribute it and/or modify it under the same
terms as the Perl 5 programming language system itself.
