#!/usr/bin/env perl
# PODNAME: tqmd_import.pl
# ABSTRACT: Script for importing custom genomes into TQMD.

use Modern::Perl '2011';
use autodie;

use Getopt::Euclid qw(:vars);
use Smart::Comments;

use Config::IniFiles;
use DBI;
use File::Basename;
use File::Find::Rule;
use File::Temp;
use List::Compare;
use LWP::Simple;
use Path::Class qw(dir file);

use Bio::MUST::Core;
use aliased 'Bio::MUST::Core::SeqId';
use aliased 'Bio::MUST::Core::Taxonomy';
use aliased 'Bio::MUST::Core::Taxonomy::Filter';

use FindBin qw($Bin); use lib "$Bin/../lib";

use TQMD qw(get_genomic get_gcas_infos_dna_custom get_list_compare);


# path for the taxdump
my $taxdir = dir($ARGV_tqmd_dir, 'taxdir');
my $tax = Taxonomy->new_from_cache( tax_dir => $taxdir );

my $custom_path = dir($ARGV_tqmd_dir, 'archives', 'custom')->stringify;
### $custom_path

### find_path
my @dnafiles = @{ get_genomic($custom_path) };

### filename
my ($rsync_gcas_ref, $file_for_ref, $path_for_ref, $fullname_for_ref)
    = get_gcas_infos_dna_custom(@dnafiles);
my @rsync_gcas = @$rsync_gcas_ref;
my %path_for = %$path_for_ref;
my %fullname_for = %$fullname_for_ref;

### check new
my $ini_cfg = new Config::IniFiles -file => "$ARGV_config";
my $dsn = $ini_cfg->val('Database', 'database');
my $dbn = $ini_cfg->val('Database', 'username');
my $dbp = $ini_cfg->val('Database', 'password');
my $dbh = DBI->connect($dsn, $dbn, $dbp)
    or die 'Couldn\'t connect to database: ' . DBI->errstr;
my @stored_gcas
    = @{ $dbh->selectcol_arrayref('SELECT gca FROM Assemblies') };

### comparaison
my $new_gcas_ref = get_list_compare(\@stored_gcas, \@rsync_gcas, 'complement');
my @new_gcas = @$new_gcas_ref;

### db_update
$dbh = DBI->connect($dsn, $dbn, $dbp)
    or die 'Couldn\'t connect to database: ' . DBI->errstr;
# Store the collection new entry
my $coll_name = $ARGV_coll_name;
my $coll_desc = 'Genomes provided by user';
my $insert_collections = $dbh->prepare_cached(
    'INSERT INTO Collections (name, description, user, date) VALUES (?,?,?,NOW())'
);
$insert_collections->execute($coll_name, $coll_desc, 'admin')
    or die 'Couldn\'t execute statement: ' . $insert_collections->errstr;
# retrieve the coll_autoid from the entry we've just created in Collections
my $last_coll_id = $insert_collections->{mysql_insertid}
    or die 'Couldn\'t execute statement: ' . $insert_collections->errstr;

# get taxonomy
my %tax_for;
my %species_for;
foreach my $gca (@new_gcas) {
    #~ my $taxon_id = $tax->get_taxid_from_name($fullname_for{$gca});
    my $seq_id = $fullname_for{$gca} . '@1';
    ### $seq_id
    my @taxonomy = $tax->fetch_lineage($seq_id);
    ### @taxonomy
    $tax_for{$gca} = \@taxonomy;
    my $species = $taxonomy[-1];
    $species_for{$gca} = $species;
}

### %species_for

# store the new Assets's entry
my $insert_assets
    = $dbh->prepare_cached('INSERT INTO Assets VALUES (?,?)');
$insert_assets->execute($last_coll_id, 'public')
    or die 'Couldn\'t execute statement: ' . $insert_assets->errstr;
# store informations for every gca in two different tables, Assemblies and Entries
my $insert_assemblies
    = $dbh->prepare_cached('INSERT INTO Assemblies VALUES (?,?,?,?)');
my $insert_entries = $dbh->prepare_cached('INSERT INTO Entries VALUES (?,?)');
my @stored_collid_entries
    = @{ $dbh->selectcol_arrayref('SELECT DISTINCT(coll_id) FROM Entries') };

foreach my $gca (@new_gcas) {
    $insert_assemblies->execute($gca, $species_for{$gca}, $path_for{$gca}, 'admin')
        or die 'Couldn\'t execute statement: ' . $insert_assemblies->errstr;
    $insert_entries->execute($gca, $last_coll_id)
        or die 'Couldn\'t execute statement: ' . $insert_entries->errstr;
}
my $insert_organisms
    = $dbh->prepare_cached('INSERT INTO Organisms VALUES (?,?)');
my $update_organisms
    = $dbh->prepare_cached('UPDATE Organisms SET lineage=? WHERE ?');
foreach my $gca (@new_gcas) {
    $insert_organisms->execute($gca, join( q{; }, @{ $tax_for{$gca} } ))
        or die 'Couldn\'t execute statement: ' . $insert_organisms->errstr;
}

__END__

=head1 VERSION

version 0.2.1

=head1 USAGE

    $ tqmd_import.pl --tqmd-dir=<dir> --config=<file> --coll-name=<str> \
        [optional arguments]

=head1 REQUIRED ARGUMENTS

=over

=item --tqmd-dir=<dir>

Full path to the place you want B<TQMD> to store the genomes and proteomes
(e.g., C</home/leonard/work/TQMD_DIR/>).

=for Euclid:
    dir.type: string

=item --coll-name=<str>

Name of the update which will be stored within the database. We advise you to
use the date in the following format for the name C<YYYY_MM_DD>.

=for Euclid:
    str.type: string

=item --config=<file>

Path to the INI file specifying TQMD configuration. If this file is not located
in the same directory used to launch B<TQMD>, add the full path before the name
(e.g., C</home/leonard/work/TQMD_DIR/tqmd_config.ini>).

=for Euclid:
    file.type: readable

=back

=head1 OPTIONAL ARGUMENTS

=over

=item --version

=item --usage

=item --help

=item --man

Print the usual program information

=back

=head1 AUTHORS

=over

=item Raphael R. LEONARD <rleonard@doct.uliege.be>

=item Denis BAURAIN <denis.baurain@uliege.be>

=back

=head1 LICENSE AND COPYRIGHT

This software is copyright (c) 2020 by University of Liege / Unit of Eukaryotic
Phylogenomics / Raphael R. LEONARD and Denis BAURAIN.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
