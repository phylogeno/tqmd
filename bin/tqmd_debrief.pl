#!/usr/bin/env perl
# PODNAME: tqmd_debrief.pl
# ABSTRACT: Script for summarizing TQMD run results.

use Modern::Perl '2011';
use autodie;

use Getopt::Euclid qw(:vars);
use Smart::Comments;

use Array::Utils qw(:all);
use File::Find::Rule;
use Path::Class qw(dir);


my $tmpdir = dir($ARGV_tqmd_dir, 'temp', $ARGV_temp_dir)->stringify;

my @interm_files = File::Find::Rule
    ->file()
    ->name( 'Interm_*_*.clusterlist' )
    ->in($tmpdir)
;

my @result_file = File::Find::Rule
    ->file()
    ->name( '*.result' )
    ->in($tmpdir)
;

### @result_file

open my $gcfs, '<', $result_file[0];
my %gcfs_listrep;
my @gcf_keys;
while (my $line = <$gcfs>) {
    chomp $line;
    push( @{ $gcfs_listrep { $line } }, $line );
    push(@gcf_keys, $line);
}

#~ ### @gcf_keys

my @sorted_interm_files = sort { $b cmp $a } @interm_files;

### @sorted_interm_files

foreach my $interm_file (@sorted_interm_files) {
    ### $interm_file
    open my $gcf_list, '<', $interm_file;
    ITEM:
    while (my $line = <$gcf_list>) {
        chomp $line;
        #~ ### $line
        my ($gcf, $rep) = split /\s+/, $line;
        #~ ### $gcf
        foreach my $key (@gcf_keys) {
            my @temp_array = @{ $gcfs_listrep{$key} };
            if ( $rep eq $key or grep( /^$rep$/, @temp_array) ) {
                push( @{ $gcfs_listrep { $key } }, $gcf);
                next ITEM;
            }
        }
    }
}

#~ ### %gcfs_listrep

my $newName = $result_file[0];
$newName =~ s/result$/debrief/g;

open (my $fh, '>', $newName);

foreach my $key (@gcf_keys) {
    my @to_trim = @{ $gcfs_listrep{$key} };
    my @uniques = unique(@to_trim);
    say $fh "$key:" . join(",", @uniques);
}

close $fh;

__END__

=head1 VERSION

version 0.2.1

=head1 USAGE

    $ tqmd_debrief.pl --tqmd-dir=<dir> --temp-dir=<dir> [optional arguments]

=head1 REQUIRED ARGUMENTS

=over

=item --tqmd-dir=<dir>

Full path to the place you want B<TQMD> to store the genomes and proteomes
(e.g., C</home/leonard/work/TQMD_DIR/>).

=for Euclid:
    dir.type: string

=item --temp-dir=<dir>

Full path to the place which will be used to store the temporary files required
by the sub-scripts to produce the data.

=for Euclid:
    dir.type: string

=back

=head1 OPTIONAL ARGUMENTS

=over

=item --version

=item --usage

=item --help

=item --man

Print the usual program information

=back

=head1 AUTHORS

=over

=item Raphael R. LEONARD <rleonard@doct.uliege.be>

=item Denis BAURAIN <denis.baurain@uliege.be>

=back

=head1 LICENSE AND COPYRIGHT

This software is copyright (c) 2020 by University of Liege / Unit of Eukaryotic
Phylogenomics / Raphael R. LEONARD and Denis BAURAIN.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
