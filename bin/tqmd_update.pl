#!/usr/bin/env perl
# PODNAME: tqmd_update.pl
# ABSTRACT: Script for updating single-genome metrics for TQMD.

use Modern::Perl '2011';
use autodie;

use Getopt::Euclid qw(:vars);
use Smart::Comments;

use Array::Split qw( split_by split_into );
use Carp;
use Config::IniFiles;
use Data::UUID;
use DBI;
use File::Basename;
use File::Find::Rule;
use File::Temp;
use IPC::System::Simple qw(system);
use List::AllUtils qw(shuffle uniq min max);
use List::Compare;
use Parallel::Batch;
use Path::Class qw(dir);
use Statistics::Data::Rank;
use Template;

use Bio::MUST::Core;
use aliased 'Bio::MUST::Core::Taxonomy';

use FindBin qw($Bin); use lib "$Bin/../lib";

use TQMD qw(get_genomic get_protein get_gcas_infos_dna get_gcas_infos_pep get_new_gcas_db get_new_gcas_file split_into_array);


my $archives_path = dir($ARGV_tqmd_dir, 'archives')->stringify;
### $archives_path
( my $tmpdiro = dir($ARGV_tqmd_dir, 'temp', $ARGV_temp_dir) )->mkpath();
my $tmpdir = $tmpdiro->stringify;
( my $rnammerdiro = dir($tmpdir, 'rnammer') )->mkpath();
my $rnammerdir = $rnammerdiro->stringify;
( my $annotationdiro = dir($tmpdir, 'annotation') )->mkpath();
my $annotationdir = $annotationdiro->stringify;
( my $jellyfishdiro = dir($tmpdir, 'jellyfish') )->mkpath();
my $jellyfishdir = $jellyfishdiro->stringify;
( my $quastdiro = dir($tmpdir, 'quast') )->mkpath();
my $quastdir = $quastdiro->stringify;
( my $checkmdiro = dir($tmpdir, 'checkm') )->mkpath();
my $checkmdir = $checkmdiro->stringify;
( my $cdhitdiro = dir($tmpdir, 'cdhitest') )->mkpath();
my $cdhitdir = $cdhitdiro->stringify;
( my $FTdiro = dir($tmpdir, 'FortyTwoDir') )->mkpath();
my $FTdir = $FTdiro->stringify;

my $ini_cfg = new Config::IniFiles -file => "$ARGV_config";
my $dsn = $ini_cfg->val('Database', 'database');
my $dbn = $ini_cfg->val('Database', 'username');
my $dbp = $ini_cfg->val('Database', 'password');
my $dbh = DBI->connect($dsn, $dbn, $dbp)
    or die 'Couldn\'t connect to database: ' . DBI->errstr;

my $limit;
my $nbr_array_ref;
my @nbr_array;

my $kc_opt = $ARGV_kmer_canonical ? '--kmer-canonical' : q{};
my $can_opt = $ARGV_kmer_canonical ? '-C' : q{};

if ($ARGV_calc eq 'jellyfish') {

    ### find_path
    my @dnafiles = @{ get_genomic($archives_path) };

    ### filename
    my ($rsync_gcas_ref, $file_for_ref, $path_for_ref) = get_gcas_infos_dna(@dnafiles);
    my @rsync_gcas = @$rsync_gcas_ref;
    my %file_for = %$file_for_ref;
    my %path_for = %$path_for_ref;

    my @jellyfish_gcas;
    my $jelly_table = $ARGV_kmer_canonical ? "JellyfishC_$ARGV_kmer_size" : "Jellyfish_$ARGV_kmer_size";
    @jellyfish_gcas = @{ $dbh->selectcol_arrayref("SELECT gca FROM $jelly_table") };
    my @new_gcas = @{ get_new_gcas_db(\@jellyfish_gcas, \@rsync_gcas) };

    ($limit, $nbr_array_ref) = split_into_array($ARGV_pack_size, \@new_gcas, "Jelly", $tmpdir);
    @nbr_array = @$nbr_array_ref;

    if ($ARGV_single_node) {
            my @array4batch;
            my $list_number = 1;
            foreach my $work_list (@nbr_array) {
                my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash

cd $ARGV_tqmd_dir

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=jellycalc --intrn-gca-list=$work_list --temp-dir=$ARGV_temp_dir --kmer-size=$ARGV_kmer_size --threads=$ARGV_threads $kc_opt
EOT

close $array_sub;
                $list_number++;
                push @array4batch, $array_sub;
            }
            ### @array4batch
            my $maxprocs_PB = min($limit, $ARGV_max_array);
            my $calc_batch = Parallel::Batch->new( {
                maxprocs => $maxprocs_PB,
                jobs => \@array4batch,
                code => sub {
                        ### launch_array
                        my $sh_file_array = shift;
                        system("sh $sh_file_array");
                        ### sh done
                    },
                progress_cb => {
                    done => sub {
                                ### Jellyfish done
                            }
                }
            } );
            ### launch_run
            $calc_batch->run();
            ### stop_run
    } else {

my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );

print {$array_sub} <<"EOT";
#!/bin/bash
#\$ -S /bin/bash
#\$ -V
#\$ -cwd
#\$ -q *s.q
#\$ -N TQMD-Jelly
#\$ -t 1-$limit
#\$ -tc $ARGV_max_array

cd $ARGV_tqmd_dir

parameter=`sed -n "\${SGE_TASK_ID} p" temp/$ARGV_temp_dir/Jelly_array`

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=jellycalc --intrn-gca-list=\${parameter} --temp-dir=$ARGV_temp_dir --kmer-size=$ARGV_kmer_size --threads=$ARGV_threads $kc_opt
EOT

close $array_sub;

system("qsub -pe snode $ARGV_threads $array_sub");
    }

} elsif ($ARGV_calc eq 'jellycalc') {

    ### find_path
    my @dnafiles = @{ get_genomic($archives_path) };

    ### filename
    my ($rsync_gcas_ref, $file_for_ref, $path_for_ref) = get_gcas_infos_dna(@dnafiles);
    my @rsync_gcas = @$rsync_gcas_ref;
    my %file_for = %$file_for_ref;
    my %path_for = %$path_for_ref;

    my ($new_gcas_ref, $taskid) = get_new_gcas_file($ARGV_intrn_gca_list);
    my @new_gcas = @$new_gcas_ref;

    my %single_size;
    my $dep_jellyfish = $ini_cfg->val('Dependencies', 'jellyfish_exe');
    foreach my $gca (@new_gcas) {
        ### $gca
        my $ug = Data::UUID->new;
        my $uuid = $ug->create();
        my $rdmstr = $ug->to_string( $uuid );
        my $file = $file_for{$gca};
        system("gunzip -d -c $file > /tmp/jellyfile_$rdmstr");
        system("$dep_jellyfish count $can_opt -m $ARGV_kmer_size -s 100M -t $ARGV_threads -o /tmp/jellystats_$rdmstr /tmp/jellyfile_$rdmstr");
        my $distids_NM = `$dep_jellyfish stats /tmp/jellystats_$rdmstr* | grep Distinct: | cut -f2 -d':' | sed -e 's/^[ \t]*//'`;
        ### $distids_NM
        chomp $distids_NM;
        ### $distids_NM
        $single_size{$gca} = $distids_NM ;
        ### size: $single_size{$gca}
        system("rm -rf /tmp/jellystats_$rdmstr*");
        system("rm -rf /tmp/jellyfile_$rdmstr");
    }
    $dbh = DBI->connect($dsn, $dbn, $dbp)
        or die 'Couldn\'t connect to database: ' . DBI->errstr;
    my $jelly_table = $ARGV_kmer_canonical ? "JellyfishC_$ARGV_kmer_size" : "Jellyfish_$ARGV_kmer_size";
    my $insert_jelly =
            $dbh->prepare_cached("INSERT INTO $jelly_table VALUES (?,?)");
    foreach my $ass (keys %single_size) {
        $insert_jelly->execute($ass, $single_size{$ass})
            or die 'Couldn\'t connect to database: ' . $insert_jelly->errstr;
    }
} elsif ($ARGV_calc eq 'quast') {

    ### find_path
    my @dnafiles = @{ get_genomic($archives_path) };

    ### filename
    my ($rsync_gcas_ref, $file_for_ref, $path_for_ref) = get_gcas_infos_dna(@dnafiles);
    my @rsync_gcas = @$rsync_gcas_ref;
    my %file_for = %$file_for_ref;
    my %path_for = %$path_for_ref;

    my @quast_gcas = @{ $dbh->selectcol_arrayref('SELECT gca FROM Quast') };
    my @new_gcas = @{ get_new_gcas_db(\@quast_gcas, \@rsync_gcas) };

    my $new_gcas_size = @new_gcas;
    ### $new_gcas_size
    ($limit, $nbr_array_ref) = split_into_array($ARGV_pack_size, \@new_gcas, "Quast", $tmpdir);
    @nbr_array = @$nbr_array_ref;

    if ($ARGV_single_node) {
        my @array4batch;
        my $list_number = 1;
        foreach my $work_list (@nbr_array) {
            my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash

cd $ARGV_tqmd_dir

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=quastcalc --intrn-gca-list=$work_list --temp-dir=$ARGV_temp_dir
EOT

close $array_sub;
            $list_number++;
            push @array4batch, $array_sub;
        }
            ### @array4batch
        my $maxprocs_PB = min($limit, $ARGV_max_array);
        my $calc_batch = Parallel::Batch->new( {
                maxprocs => $maxprocs_PB,
                jobs => \@array4batch,
                code => sub {
                        ### launch_array
                        my $sh_file_array = shift;
                        system("sh $sh_file_array");
                        ### sh done
                    },
                progress_cb => {
                    done => sub {
                                ### Quast done
                            }
            }
        } );
        ### launch_run
        $calc_batch->run();
        ### stop_run
    } else {

my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash
#\$ -S /bin/bash
#\$ -V
#\$ -cwd
#\$ -q *s.q
#\$ -N TQMD-Quast
#\$ -t 1-$limit
#\$ -tc $ARGV_max_array

cd $ARGV_tqmd_dir

parameter=`sed -n "\${SGE_TASK_ID} p" temp/$ARGV_temp_dir/Quast_array`

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=quastcalc --intrn-gca-list=\${parameter} --temp-dir=$ARGV_temp_dir
EOT


close $array_sub;

system("qsub -pe snode $ARGV_threads $array_sub");
    }

} elsif ($ARGV_calc eq 'quastcalc') {

    ### find_path
    my @dnafiles = @{ get_genomic($archives_path) };

    ### filename
    my ($rsync_gcas_ref, $file_for_ref, $path_for_ref) = get_gcas_infos_dna(@dnafiles);
    my @rsync_gcas = @$rsync_gcas_ref;
    my %file_for = %$file_for_ref;
    my %path_for = %$path_for_ref;

    my ($new_gcas_ref, $taskid) = get_new_gcas_file($ARGV_intrn_gca_list);
    my @new_gcas = @$new_gcas_ref;

    my %quast_vals;
    my $dep_quast = $ini_cfg->val('Dependencies', 'quast_exe');
    foreach my $gcf (@new_gcas) {
        ### $gcf
        my $ug = Data::UUID->new;
        my $uuid = $ug->create();
        my $rdmstr = $ug->to_string( $uuid );
        my $ret_code = system("$dep_quast -m 1000 -o $quastdir/quastcalc_$rdmstr $file_for{$gcf} --silent --no-plots --no-html --no-icarus --no-snps --no-sv --no-gzip --threads $ARGV_threads --contig-thresholds 0,1000");
        if ($ret_code != 0) {
            carp 'Warning: cannot execute quast command; returning without parser!';
            next;
        } else {
            if (open my $report, '<', "$quastdir/quastcalc_$rdmstr/transposed_report.tsv") {
                while (my $line = <$report>) {
                    chomp $line;
                    my ($gca, @values) = split /\t/xms, $line;
                    next if $gca =~ m/Assembly|_broken/ ;
                    $quast_vals{$gcf} = \@values;
                }
            }
        }
        system("rm -rf /$quastdir/quastcalc_$rdmstr/");
    }
    $dbh = DBI->connect($dsn, $dbn, $dbp)
        or die 'Couldn\'t connect to database: ' . DBI->errstr;
    my $insert_quast =
        $dbh->prepare_cached('INSERT INTO Quast VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
    foreach my $ass (keys %quast_vals) {
        $insert_quast->execute($ass, $quast_vals{$ass}[0], $quast_vals{$ass}[1], $quast_vals{$ass}[2],
         $quast_vals{$ass}[3], $quast_vals{$ass}[4], $quast_vals{$ass}[5], $quast_vals{$ass}[6],
          $quast_vals{$ass}[7], $quast_vals{$ass}[8], $quast_vals{$ass}[9], $quast_vals{$ass}[10],
           $quast_vals{$ass}[11], $quast_vals{$ass}[12],)
        or die 'Couldn\'t connect to database: ' . $insert_quast->errstr;
        }
} elsif ($ARGV_calc eq 'rnammer') {

    ### find_path
    my @dnafiles = @{ get_genomic($archives_path) };

    ### filename
    my ($rsync_gcas_ref, $file_for_ref, $path_for_ref) = get_gcas_infos_dna(@dnafiles);
    my @rsync_gcas = @$rsync_gcas_ref;
    my %file_for = %$file_for_ref;
    my %path_for = %$path_for_ref;

    my @RNA_gcas = @{ $dbh->selectcol_arrayref('SELECT gca FROM RNA16s') };
    my @new_gcas = @{ get_new_gcas_db(\@RNA_gcas, \@rsync_gcas) };

    my $new_gcas_size = @new_gcas;
    ### $new_gcas_size

    ($limit, $nbr_array_ref) = split_into_array($ARGV_pack_size, \@new_gcas, "RNAmmer", $tmpdir);
    @nbr_array = @$nbr_array_ref;

    if ($ARGV_single_node) {
        my @array4batch;
            my $list_number = 1;
            foreach my $work_list (@nbr_array) {
                my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash

cd $ARGV_tqmd_dir

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=16scalc --intrn-gca-list=$work_list --temp-dir=$ARGV_temp_dir
EOT

close $array_sub;
                $list_number++;
                push @array4batch, $array_sub;
            }
            ### @array4batch
            my $maxprocs_PB = min($limit, $ARGV_max_array);
            my $calc_batch = Parallel::Batch->new( {
                maxprocs => $maxprocs_PB,
                jobs => \@array4batch,
                code => sub {
                        ### launch_array
                        my $sh_file_array = shift;
                        system("sh $sh_file_array");
                        ### sh done
                    },
                progress_cb => {
                    done => sub {
                                ### rnammer done
                            }
                }
            } );
            ### launch_run
            $calc_batch->run();
            ### stop_run
    } else {

my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash
#\$ -S /bin/bash
#\$ -V
#\$ -cwd
#\$ -q *s.q
#\$ -N TQMD-16s
#\$ -t 1-$limit
#\$ -tc $ARGV_max_array

cd $ARGV_tqmd_dir

parameter=`sed -n "\${SGE_TASK_ID} p" temp/$ARGV_temp_dir/RNAmmer_array`

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=16scalc --intrn-gca-list=\${parameter} --temp-dir=$ARGV_temp_dir
EOT


close $array_sub;

system("qsub -pe snode $ARGV_threads $array_sub");
    }

} elsif ($ARGV_calc eq '16scalc') {

    ### find_path
    my @dnafiles = @{ get_genomic($archives_path) };

    ### filename
    my ($rsync_gcas_ref, $file_for_ref, $path_for_ref) = get_gcas_infos_dna(@dnafiles);
    my @rsync_gcas = @$rsync_gcas_ref;
    my %file_for = %$file_for_ref;
    my %path_for = %$path_for_ref;

    my ($new_gcas_ref, $taskid) = get_new_gcas_file($ARGV_intrn_gca_list);
    my @new_gcas = @$new_gcas_ref;

    my %RNA_seqs;
    my $dep_rnammer = $ini_cfg->val('Dependencies', 'rnammer_exe');
    foreach my $gcf (@new_gcas) {
        ### $gcf
        my $ug = Data::UUID->new;
        my $uuid = $ug->create();
        my $rdmstr = $ug->to_string( $uuid );
        system("gunzip -d -c $file_for{$gcf} > $rnammerdir/test_16s_$rdmstr.fasta");
        system("$dep_rnammer -S bac -m ssu -d -f $rnammerdir/test_16s_$rdmstr.fna < $rnammerdir/test_16s_$rdmstr.fasta");
        system("fasta2ali.pl $rnammerdir/test_16s_$rdmstr.fna");
        open my $seqs, '<', "$rnammerdir/test_16s_$rdmstr.ali";
        my $last_id;
        while (my $line = <$seqs>) {
            chomp $line;
            ### $line
            next if ($line =~ m/#/);
            if ($line =~ m/^>/) {
                my (undef, $line_id) = split />/xms, $line;
                ($last_id, undef) = split /\s+/xms, $line_id;
                ### $last_id
            } else {
                $RNA_seqs{$gcf}{$last_id} = $line;
            }
        }
        close $seqs;
        system("rm -rf $rnammerdir/test_16s_$rdmstr.*");
    }
    $dbh = DBI->connect($dsn, $dbn, $dbp)
        or die 'Couldn\'t connect to database: ' . DBI->errstr;
    my $insert_rna16s =
        $dbh->prepare_cached('INSERT INTO RNA16s VALUES (?,?,?)');
    foreach my $ass (keys %RNA_seqs) {
        foreach my $rnaid (keys %{$RNA_seqs{$ass}} ) {
            $insert_rna16s->execute($ass, $rnaid, $RNA_seqs{$ass}{$rnaid})
            or die 'Couldn\'t connect to database: ' . $insert_rna16s->errstr;
        }
    }

} elsif ($ARGV_calc eq 'annotation') {

    ### find_path
    my @pepfiles = @{ get_protein($archives_path) };

    ### filename
    my ($rsync_gcas_ref, $file_for_ref, $path_for_ref) = get_gcas_infos_pep(@pepfiles);
    my @rsync_gcas = @$rsync_gcas_ref;
    my %file_for = %$file_for_ref;
    my %path_for = %$path_for_ref;

    my @Ann_gcas = @{ $dbh->selectcol_arrayref('SELECT gca FROM Annotations') };
    my @new_gcas = @{ get_new_gcas_db(\@Ann_gcas, \@rsync_gcas) };

    my $new_gcas_size = @new_gcas;
    ### $new_gcas_size

    ($limit, $nbr_array_ref) = split_into_array($ARGV_pack_size, \@new_gcas, "Annot", $tmpdir);
    @nbr_array = @$nbr_array_ref;

    if ($ARGV_single_node) {
        my @array4batch;
        my $list_number = 1;
        foreach my $work_list (@nbr_array) {
my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash

cd $ARGV_tqmd_dir

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=anncalc --intrn-gca-list=$work_list --temp-dir=$ARGV_temp_dir
EOT

close $array_sub;
                $list_number++;
                push @array4batch, $array_sub;
            }
            ### @array4batch
            my $maxprocs_PB = min($limit, $ARGV_max_array);
            my $calc_batch = Parallel::Batch->new( {
                maxprocs => $maxprocs_PB,
                jobs => \@array4batch,
                code => sub {
                        ### launch_array
                        my $sh_file_array = shift;
                        system("sh $sh_file_array");
                        ### sh done
                    },
                progress_cb => {
                    done => sub {
                                ### Annotation done
                            }
                }
            } );
            ### launch_run
            $calc_batch->run();
            ### stop_run
    } else {

my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash
#\$ -S /bin/bash
#\$ -V
#\$ -cwd
#\$ -q *s.q
#\$ -N TQMD-Ann
#\$ -t 1-$limit
#\$ -tc $ARGV_max_array

cd $ARGV_tqmd_dir

parameter=`sed -n "\${SGE_TASK_ID} p" temp/$ARGV_temp_dir/Annot_array`

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=anncalc --intrn-gca-list=\${parameter} --temp-dir=$ARGV_temp_dir
EOT


close $array_sub;

system("qsub -pe snode $ARGV_threads $array_sub");
    }

} elsif ($ARGV_calc eq 'anncalc') {

    ### find_path
    my @pepfiles = @{ get_protein($archives_path) };

    ### filename
    my ($rsync_gcas_ref, $file_for_ref, $path_for_ref) = get_gcas_infos_pep(@pepfiles);
    my @rsync_gcas = @$rsync_gcas_ref;
    my %file_for = %$file_for_ref;
    my %path_for = %$path_for_ref;

    my ($new_gcas_ref, $taskid) = get_new_gcas_file($ARGV_intrn_gca_list);
    my @new_gcas = @$new_gcas_ref;

    my %Ann_seqs;
    my %Com_seqs;
    foreach my $gcf (@new_gcas) {
        ### $gcf
        my $ug = Data::UUID->new;
        my $uuid = $ug->create();
        my $rdmstr = $ug->to_string( $uuid );
        system("gunzip -d -c $file_for{$gcf} > $annotationdir/Annot_$rdmstr.fasta");
        open my $seqs, '<', "$annotationdir/Annot_$rdmstr.fasta";
        my $last_id;
        my $total_count = 0;
        my $undef_count = 0;
        my $void_count = 0;
        while (my $line = <$seqs>) {
            chomp $line;
            ### $line
            if ($line =~ m/^>/) {
                $total_count++;
                $void_count++ if $line =~ m/^\>\S+ \[/i;
                $undef_count++ if $line =~ m/unknown|hypothetical|predicted|undefined|uncharacterized|putative|probable/i;
            }
            my $ratio = 1 - ($undef_count/$total_count);
            my $complt_ratio = 1 - ($void_count/$total_count);
            $Ann_seqs{$gcf} = $ratio;
            $Com_seqs{$gcf} = $complt_ratio;
        }
        close $seqs;
        system("rm -rf $annotationdir/Annot_$rdmstr.fasta");
    }
    $dbh = DBI->connect($dsn, $dbn, $dbp)
        or die 'Couldn\'t connect to database: ' . DBI->errstr;
    my $insert_ann =
        $dbh->prepare_cached('INSERT INTO Annotations VALUES (?,?,?)');
    foreach my $ass (keys %Ann_seqs) {
        $insert_ann->execute($ass, $Ann_seqs{$ass}, $Com_seqs{$ass})
        or die 'Couldn\'t connect to database: ' . $insert_ann->errstr;
    }

} elsif ($ARGV_calc eq 'checkm') {
    ### find_path
    my @dnafiles = @{ get_genomic($archives_path) };

    ### filename
    my ($rsync_gcas_ref, $file_for_ref, $path_for_ref) = get_gcas_infos_dna(@dnafiles);
    my @rsync_gcas = @$rsync_gcas_ref;
    my %file_for = %$file_for_ref;
    my %path_for = %$path_for_ref;

    my @CM_gcas = @{ $dbh->selectcol_arrayref('SELECT gca FROM CheckM') };
    my @new_gcas = @{ get_new_gcas_db(\@CM_gcas, \@rsync_gcas) };

    my $new_gcas_size = @new_gcas;
    ### $new_gcas_size

    ($limit, $nbr_array_ref) = split_into_array($ARGV_pack_size, \@new_gcas, "CheckM", $tmpdir);
    @nbr_array = @$nbr_array_ref;

    if ($ARGV_single_node) {
        my @array4batch;
        my $list_number = 1;
        foreach my $work_list (@nbr_array) {
my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash

cd $ARGV_tqmd_dir

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=checkmcalc --intrn-gca-list=$work_list --temp-dir=$ARGV_temp_dir --threads=$ARGV_threads
EOT

close $array_sub;
                $list_number++;
                push @array4batch, $array_sub;
            }
            ### @array4batch
            my $maxprocs_PB = min($limit, $ARGV_max_array);
            my $calc_batch = Parallel::Batch->new( {
                maxprocs => $maxprocs_PB,
                jobs => \@array4batch,
                code => sub {
                        ### launch_array
                        my $sh_file_array = shift;
                        system("sh $sh_file_array");
                        ### sh done
                    },
                progress_cb => {
                    done => sub {
                                ### chekm done
                            }
                }
            } );
            ### launch_run
            $calc_batch->run();
            ### stop_run
    } else {

my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash
#\$ -S /bin/bash
#\$ -V
#\$ -cwd
#\$ -q *s.q
#\$ -N TQMD-CM
#\$ -t 1-$limit
#\$ -tc $ARGV_max_array

cd $ARGV_tqmd_dir

parameter=`sed -n "\${SGE_TASK_ID} p" temp/$ARGV_temp_dir/CheckM_array`

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=checkmcalc --intrn-gca-list=\${parameter} --temp-dir=$ARGV_temp_dir --threads=$ARGV_threads
EOT


close $array_sub;

system("qsub -pe snode $ARGV_threads $array_sub");
    }

} elsif ($ARGV_calc eq 'checkmcalc') {
    ### find_path
    my @dnafiles = @{ get_genomic($archives_path) };

    ### filename
    my ($rsync_gcas_ref, $file_for_ref, $path_for_ref) = get_gcas_infos_dna(@dnafiles);
    my @rsync_gcas = @$rsync_gcas_ref;
    my %file_for = %$file_for_ref;
    my %path_for = %$path_for_ref;

    my ($new_gcas_ref, $taskid) = get_new_gcas_file($ARGV_intrn_gca_list);
    my @new_gcas = @$new_gcas_ref;

    my %checkm_cmpl;
    my %checkm_conta;
    my %checkm_htrg;
    my $dep_checkm = $ini_cfg->val('Dependencies', 'checkm_exe');
    foreach my $gcf (@new_gcas) {
        ### $gcf
        my $ug = Data::UUID->new;
        my $uuid = $ug->create();
        my $rdmstr = $ug->to_string( $uuid );
        system("mkdir $checkmdir/checkmin_$rdmstr");
        system("mkdir $checkmdir/checkmout_$rdmstr");
        system("gunzip -d -c $file_for{$gcf} > $checkmdir/checkmin_$rdmstr/file_checkm_$rdmstr.fna");
        system("$dep_checkm lineage_wf -f $checkmdir/CM_results_$rdmstr.list --tab_table -t $ARGV_threads -x fna $checkmdir/checkmin_$rdmstr/ $checkmdir/checkmout_$rdmstr");
        open my $result, '<', "$checkmdir/CM_results_$rdmstr.list";
        while (my $line = <$result>) {
            chomp $line;
            ### $line
            next if /^Bin Id  Marker lineage/;
            my @values = split /\t/xms, $line;
            $checkm_htrg{$gcf} = $values[-1];
            $checkm_conta{$gcf} = $values[-2];
            $checkm_cmpl{$gcf} = $values[-3];
        }
        close $result;
        system("rm -rf $checkmdir/checkmin_$rdmstr");
        system("rm -rf $checkmdir/checkmout_$rdmstr");
    }
    $dbh = DBI->connect($dsn, $dbn, $dbp)
        or die 'Couldn\'t connect to database: ' . DBI->errstr;
    my $insert_checkm =
        $dbh->prepare_cached('INSERT INTO CheckM VALUES (?,?,?,?)');
    foreach my $ass (@new_gcas) {
        $insert_checkm->execute($ass, $checkm_cmpl{$ass}, $checkm_conta{$ass}, $checkm_htrg{$ass})
            or die 'Couldn\'t connect to database: ' . $insert_checkm->errstr;
    }

} elsif ($ARGV_calc eq 'cdhit') {
    my @RNA_gcas_raw = @{ $dbh->selectcol_arrayref('SELECT gca FROM RNA16s') };
    my @RNA_gcas = uniq @RNA_gcas_raw;
    my $RNA_gcas_size = @RNA_gcas;
    ### $RNA_gcas_size

    ($limit, $nbr_array_ref) = split_into_array($ARGV_pack_size, \@RNA_gcas, "CdHitEst", $tmpdir);
    @nbr_array = @$nbr_array_ref;

    if ($ARGV_single_node) {
        my @array4batch;
        my $list_number = 1;
        foreach my $work_list (@nbr_array) {
my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash

cd $ARGV_tqmd_dir

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=cdhitcalc --intrn-gca-list=$work_list --temp-dir=$ARGV_temp_dir --threads=$ARGV_threads --cdhit-threshold=$ARGV_cdhit_threshold
EOT

close $array_sub;
        $list_number++;
        push @array4batch, $array_sub;
    }
        ### @array4batch
        my $maxprocs_PB = min($limit, $ARGV_max_array);
        my $calc_batch = Parallel::Batch->new( {
            maxprocs => $maxprocs_PB,
            jobs => \@array4batch,
            code => sub {
                    ### launch_array
                    my $sh_file_array = shift;
                    system("sh $sh_file_array");
                    ### sh done
                },
            progress_cb => {
                done => sub {
                            ### cdhit done
                        }
            }
        } );
        ### launch_run
        $calc_batch->run();
        ### stop_run
    } else {

my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash
#\$ -S /bin/bash
#\$ -V
#\$ -cwd
#\$ -q *s.q
#\$ -N TQMD-CHE
#\$ -t 1-$limit
#\$ -tc $ARGV_max_array

cd $ARGV_tqmd_dir

parameter=`sed -n "\${SGE_TASK_ID} p" temp/$ARGV_temp_dir/CdHitEst_array`

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=cdhitcalc --intrn-gca-list=\${parameter} --temp-dir=$ARGV_temp_dir --threads=$ARGV_threads --cdhit-threshold=$ARGV_cdhit_threshold
EOT


close $array_sub;

system("qsub -pe snode $ARGV_threads $array_sub");
    }

} elsif ($ARGV_calc eq 'cdhitcalc') {
    my ($new_gcas_ref, $taskid) = get_new_gcas_file($ARGV_intrn_gca_list);
    my @new_gcas = @$new_gcas_ref;

    ### connection
    $dbh = DBI->connect($dsn, $dbn, $dbp)
        or die 'Couldn\'t connect to database: ' . DBI->errstr;

    ### fasta_creation
    my $RNAs_fasta = "$cdhitdir/TQMD_RNA16s_prep_$taskid.fasta";
    open (my $fh1, '>', $RNAs_fasta);

    foreach my $gca (@new_gcas) {
        my $sthCHE = $dbh->prepare('SELECT * FROM RNA16s WHERE gca = ?');
        $sthCHE->execute($gca);
        while (my $result = $sthCHE->fetchrow_hashref) {
            my $rnaid = $result->{'rna_id'};
            #~ ### $rnaid
            my $rnaseq = $result->{'rna_seq'};
            say $fh1 ">". $gca . "@". $rnaid . "\n" . $rnaseq ;
        }
    }

    close $fh1;

    ### cdhitest
    my $dep_cdhitest = $ini_cfg->val('Dependencies', 'cdhitest_exe');
    system("$dep_cdhitest -c $ARGV_cdhit_threshold -i $RNAs_fasta -o $cdhitdir/TQMD_RNA16s_calc_$taskid.fasta");

    system("rm -rf $RNAs_fasta");

    ### result
    open my $genomes, '<', "$cdhitdir/TQMD_RNA16s_calc_$taskid.fasta.clstr";
    my $current_cluster;
    my %cdhit_res;
    while (my $line = <$genomes>) {
        chomp $line;
        #~ ### $line
        if ($line =~ /^>/) {
            (undef, $current_cluster) = split />/, $line;
        } else {
            my (undef, $line_p1) = split />/, $line;
            my ($gca, undef) = split /@/, $line_p1;
            push( @{ $cdhit_res { $gca } }, $current_cluster );
        }
    }

    my $BAD_RNA_list = "$tmpdir/TQMD_RNA16s_BAD_$taskid.list";
    open (my $fh2, '>', $BAD_RNA_list);

    foreach my $key (keys %cdhit_res){
        my @to_trim = @{ $cdhit_res{$key} };
        #~ ### @to_trim
        my @uniques = uniq(@to_trim);
        #~ ### @uniques
        if ( (scalar @uniques) > 1 ) {
            say $fh2 $key ;
        }
    }
    close $fh2;
    system("rm -rf $cdhitdir/TQMD_RNA16s_calc_$taskid.fasta*");
} elsif ($ARGV_calc eq "fortytwo") {
    my $FTini_cfg = new Config::IniFiles -file => "$ARGV_config_42";
    my $path_refbanks = $FTini_cfg->val('Config', 'ref_bank_dir');
    ### $path_refbanks

    my @refbank_files = File::Find::Rule
        ->file()
        ->name( '*.psq' )
        ->in($path_refbanks)
    ;

    my $refbank_mapper_idm = "$FTdir/refbank_mapper.idm";
    open (my $fh_rbm, '>', $refbank_mapper_idm);

    foreach my $bank (@refbank_files) {
        my $filename = basename($bank, ".psq");
        say $fh_rbm "$filename\t$bank";
    }
    close $fh_rbm;

    ### find_path
    my @pepfiles = @{ get_protein($archives_path) };

    ### filename
    my ($rsync_gcas_ref, $file_for_ref, $path_for_ref) = get_gcas_infos_pep(@pepfiles);
    my @rsync_gcas = @$rsync_gcas_ref;
    my %file_for = %$file_for_ref;
    my %path_for = %$path_for_ref;

    my @Ann_gcas = @{ $dbh->selectcol_arrayref('SELECT gca FROM FT_result') };
    my @new_gcas = @{ get_new_gcas_db(\@Ann_gcas, \@rsync_gcas) };

    my $new_gcas_size = @new_gcas;
    ### $new_gcas_size

    ($limit, $nbr_array_ref) = split_into_array($ARGV_pack_size, \@new_gcas, "FT", $tmpdir);
    @nbr_array = @$nbr_array_ref;

    if ($ARGV_single_node) {
        my @array4batch;
        my $list_number = 1;
        foreach my $work_list (@nbr_array) {
my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash

cd $ARGV_tqmd_dir

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=FTprep --intrn-gca-list=$work_list --temp-dir=$ARGV_temp_dir --config-42=$ARGV_config_42 --single-node --max-array=$ARGV_max_array
EOT

close $array_sub;
                $list_number++;
                push @array4batch, $array_sub;
            }
            #~ ### @array4batch
            my $maxprocs_PB = min($limit, $ARGV_max_array);
            my $calc_batch = Parallel::Batch->new( {
                maxprocs => $maxprocs_PB,
                jobs => \@array4batch,
                code => sub {
                        ### launch array prep
                        my $sh_file_array = shift;
                        system("sh $sh_file_array");
                        ### sh prep done
                    },
                progress_cb => {
                    done => sub {
                                ### FT done
                            }
                }
            } );
            ### launch_run
            $calc_batch->run();
            ### stop_run
    } else {

my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash
#\$ -S /bin/bash
#\$ -V
#\$ -cwd
#\$ -q bignode.q
#\$ -N TQMD-FTp
#\$ -t 1-$limit
#\$ -tc $ARGV_max_array

cd $ARGV_tqmd_dir

parameter=`sed -n "\${SGE_TASK_ID} p" temp/$ARGV_temp_dir/FT_array`

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=FTprep --intrn-gca-list=\${parameter} --temp-dir=$ARGV_temp_dir --config-42=$ARGV_config_42 --max-array=$ARGV_max_array
EOT


close $array_sub;

system("qsub -pe snode $ARGV_threads $array_sub");
    }
} elsif ($ARGV_calc eq "FTprep") {
    ### INI prep
    my $FTini_cfg = new Config::IniFiles -file => "$ARGV_config_42";
    my $ribo_path = $FTini_cfg->val('Config', 'ribo_dir');
    ### $ribo_path
    my $path_queries = $FTini_cfg->val('Config', 'queries');
    ### $path_queries
    my $path_refbanks = $FTini_cfg->val('Config', 'ref_bank_dir');
    ### $path_refbanks
    my $path_refbanks_mapper = "$FTdir/refbank_mapper.idm";
    ### $path_refbanks_mapper
    my $ref_org_mul = $FTini_cfg->val('Config', 'ref_org_mul');
    ### $ref_org_mul
    my $taxdir = "$ARGV_tqmd_dir" . 'taxdir';
    ### $taxdir
    ### $ARGV_max_array

    ### find_path
    my @pepfiles = @{ get_protein($archives_path) };

    ### filename
    my ($rsync_gcas_ref, $file_for_ref, $path_for_ref) = get_gcas_infos_pep(@pepfiles);
    my @rsync_gcas = @$rsync_gcas_ref;
    my %file_for = %$file_for_ref;
    my %path_for = %$path_for_ref;

    my ($new_gcas_ref, $taskid) = get_new_gcas_file($ARGV_intrn_gca_list);
    ### $ARGV_intrn_gca_list
    ### $taskid
    my @new_gcas = @$new_gcas_ref;

    ### banks and mapper creation
    system("mkdir -p $FTdir/TQMD_FT_prep_$taskid/banks/");

    my $bank_mapper_idm = "$FTdir/TQMD_FT_prep_$taskid/banks/bank_mapper.idm";
    open (my $fh1, '>', $bank_mapper_idm);

    my $dep_makeblastdb = $ini_cfg->val('Dependencies', 'makeblastdb_exe');
    foreach my $gcf (@new_gcas) {
        #~ ### $gcf
        system("gunzip -d -c $file_for{$gcf} > $FTdir/TQMD_FT_prep_$taskid/banks/$gcf.fasta");
        system("$dep_makeblastdb -in $FTdir/TQMD_FT_prep_$taskid/banks/$gcf.fasta -out $FTdir/TQMD_FT_prep_$taskid/banks/$gcf -dbtype prot -parse_seqids");
        say $fh1 "$gcf\t$gcf.psq";
        system("rm -rf $FTdir/TQMD_FT_prep_$taskid/banks/$gcf.fasta");
    }
    close $fh1;

    ### banks finished

    my $ug = Data::UUID->new;
    my $uuid = $ug->create();
    my $rdmstr = $ug->to_string( $uuid );

    my @ribo_work_files = File::Find::Rule
        ->file()
        ->name( '*.ali' )
        ->in($ribo_path)
    ;

    my $ribo_file;
    open (my $ribo_file_creation, '>', "$FTdir/ribo_file_$taskid.list");
    foreach my $ribo_file_path (@ribo_work_files) {
        say $ribo_file_creation "$ribo_file_path";
    }
    close $ribo_file_creation;

    my $ribo_number = @ribo_work_files;

    if ($ARGV_single_node) {
        # yaml creation
my $yaml_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh');
print {$yaml_sub} <<"EOT";
#!/bin/bash

cd $FTdir/TQMD_FT_prep_$taskid/

yaml-generator-42.pl --run_mode=metagenomic --out_suffix=-42-tqmd-$taskid \\
--queries $path_queries \\
--evalue=1e-3 --homologues_seg=yes --max_target_seqs=50 --templates_seg=no \\
--bank_dir $FTdir/TQMD_FT_prep_$taskid/banks/ --bank_suffix=.psq --bank_mapper $FTdir/TQMD_FT_prep_$taskid/banks/bank_mapper.idm \\
--ref_brh=on --ref_bank_dir $path_refbanks --ref_bank_suffix=.psq --ref_bank_mapper $path_refbanks_mapper \\
--ref_org_mul=$ref_org_mul --ref_score_mul=0.99 \\
--trim_homologues=off \\
--merge_orthologues=off \\
--aligner_mode=blast --ali_skip_self=on --ali_cover_mul=1.1 --ali_keep_old_new_tags=on --ali_keep_lengthened_seqs=keep \\
--tax_reports=on --tax_dir $taxdir \\
--tax_min_score=80 --tax_score_mul=0.99 --tax_min_ident=0.50 --tax_min_len=0 \\
--tol_check=off
EOT

close $yaml_sub;
        ### yaml single-node launch
        system("sh $yaml_sub");
        ### yaml single-node stop

        my @array4batch;
        my $list_number = 1;
        foreach my $work_list (@ribo_work_files) {
my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash

cd $ARGV_tqmd_dir

forty-two.pl $work_list --config=$FTdir/TQMD_FT_prep_$taskid/config-42-tqmd-$taskid.yaml --outdir=$FTdir/ --verbosity=5
EOT

close $array_sub;
                $list_number++;
                push @array4batch, $array_sub;
            }

my $array_sub_2 = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub_2} <<"EOT";
#!/bin/bash

cd $FTdir/TQMD_FT_prep_$taskid/

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=FTwrap --temp-dir=$ARGV_temp_dir --config-42=$ARGV_config_42 --intrn-FT-task=$taskid
EOT

close $array_sub_2;
            #~ ### @array4batch
            my $maxprocs_PB = min($ribo_number, $ARGV_max_array);
            my $calc_batch = Parallel::Batch->new( {
                maxprocs => $maxprocs_PB,
                jobs => \@array4batch,
                code => sub {
                        ### launch_array calc
                        my $sh_file_array = shift;
                        system("sh $sh_file_array");
                        ### sh calc done
                    },
                progress_cb => {
                    done => sub {
                                ### launch wrap
                                system("sh $array_sub_2");
                                ### finish wrap
                            }
                }
            } );
            ### launch_run
            $calc_batch->run();
            ### stop_run

    } else {

    # yaml creation
my $yaml_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh');
print {$yaml_sub} <<"EOT";
#!/bin/bash
#\$ -S /bin/bash
#\$ -V
#\$ -cwd
#\$ -q *s.q
#\$ -N TQMD-FTyaml-$rdmstr

cd $FTdir/TQMD_FT_prep_$taskid/

yaml-generator-42.pl --run_mode=metagenomic --out_suffix=-42-tqmd-$taskid \\
--queries $path_queries \\
--evalue=1e-3 --homologues_seg=yes --max_target_seqs=50 --templates_seg=no \\
--bank_dir $FTdir/TQMD_FT_prep_$taskid/banks/ --bank_suffix=.psq --bank_mapper $FTdir/TQMD_FT_prep_$taskid/banks/bank_mapper.idm \\
--ref_brh=on --ref_bank_dir $path_refbanks --ref_bank_suffix=.psq --ref_bank_mapper $path_refbanks_mapper \\
--ref_org_mul=$ref_org_mul --ref_score_mul=0.99 \\
--trim_homologues=off \\
--merge_orthologues=off \\
--aligner_mode=blast --ali_skip_self=on --ali_cover_mul=1.1 --ali_keep_old_new_tags=on --ali_keep_lengthened_seqs=keep \\
--tax_reports=on --tax_dir $taxdir \\
--tax_min_score=80 --tax_score_mul=0.99 --tax_min_ident=0.50 --tax_min_len=0 \\
--tol_check=off
EOT

close $yaml_sub;

    # FT computation

my $array_sub_1 = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub_1} <<"EOT";
#!/bin/bash
#\$ -S /bin/bash
#\$ -V
#\$ -cwd
#\$ -q *s.q
#\$ -N TQMD-FTcalc-$taskid-$rdmstr
#\$ -t 1-$ribo_number
#\$ -tc $ARGV_max_array

cd $ARGV_tqmd_dir

parameter=`sed -n "\${SGE_TASK_ID} p" $FTdir/ribo_file_$taskid.list`

forty-two.pl \${parameter} --config=$FTdir/TQMD_FT_prep_$taskid/config-42-tqmd-$taskid.yaml --outdir=$FTdir --verbosity=5
EOT


close $array_sub_1;

my $array_sub_2 = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub_2} <<"EOT";
#!/bin/bash
#\$ -S /bin/bash
#\$ -V
#\$ -cwd
#\$ -q bignode.q
#\$ -N TQMD-FTw-$taskid-$rdmstr

cd $FTdir/TQMD_FT_prep_$taskid/

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --calc=FTwrap --temp-dir=$ARGV_temp_dir --config-42=$ARGV_config_42 --intrn-FT-task=$taskid
EOT


close $array_sub_2;

system("qsub -pe snode $ARGV_threads $yaml_sub");
system("qsub -hold_jid TQMD-FTyaml-$rdmstr $array_sub_1");
system("qsub -hold_jid TQMD-FTcalc-$taskid-$rdmstr $array_sub_2");

    }

} elsif ($ARGV_calc eq "FTwrap") {
    ### INI
    my $FTini_cfg = new Config::IniFiles -file => "$ARGV_config_42";
    my $seq_labeling = $FTini_cfg->val('Config', 'seq_labeling');
    ### $seq_labeling
    my $contam_labeling = $FTini_cfg->val('Config', 'contam_labeling');
    ### $contam_labeling
    my $ribo_path = $FTini_cfg->val('Config', 'ribo_dir');
    ### $ribo_path
    my $taxdir = "$ARGV_tqmd_dir" . 'taxdir';
    ### $taxdir

    system("debrief-42.pl --indir=$FTdir/ --in=-42-tqmd-$ARGV_intrn_FT_task --taxdir=$taxdir --seq_labeling=$seq_labeling --contam_labeling=$contam_labeling  --verbosity=4");

    open my $genomes, '<', "$FTdir/TQMD_FT_prep_$ARGV_intrn_FT_task/per-genome-42-tqmd-$ARGV_intrn_FT_task.stats-sum";
    my %enrich_ali;
    my %clean_ali;
    my %contam_ali;
    my %added_seq;
    my %clean_seq;
    my %contam_seq;
    my %foreign_phyla;
    my %global_contam;
    my %contam_perc;
    my %unclassified_contam_perc;
    my %unknown_perc;
    my @gcas;
    while (my $line = <$genomes>) {
        chomp $line;
        ### $line
        if ($line =~ /^bank\t/) {
            #~ next $line;
            ### skip_header
        } else {
            my @elems = split /\t/, $line;
            my $gca = $elems[0];
            my $en_ali = $elems[2];
            my $cl_ali = $elems[3];
            my $co_ali = $elems[4];
            my $ad_seq = $elems[6];
            my $cl_seq = $elems[7];
            my $co_seq = $elems[8];
            my $fo_phyla = $elems[-5];
            my $gl_contam = $elems[-4];
            my $co_perc = $elems[-3];
            my $un_contam = $elems[-2];
            my $un_perc = $elems[-1];
            push @gcas, $gca;
            $enrich_ali{$gca} = $en_ali;
            $clean_ali{$gca} = $cl_ali;
            $contam_ali{$gca} = $co_ali;
            $added_seq{$gca} = $ad_seq;
            $clean_seq{$gca} = $cl_seq;
            $contam_seq{$gca} = $co_seq;
            $foreign_phyla{$gca} = $fo_phyla;
            $global_contam{$gca} = $gl_contam;
            $contam_perc{$gca} = $co_perc;
            $unclassified_contam_perc{$gca} = $un_contam;
            $unknown_perc{$gca} = $un_perc;
        }
    }
    $dbh = DBI->connect($dsn, $dbn, $dbp)
        or die 'Couldn\'t connect to database: ' . DBI->errstr;
    my $insert_checkm =
        $dbh->prepare_cached('INSERT INTO FT_result VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
    foreach my $ass (@gcas) {
        $insert_checkm->execute($ass, $enrich_ali{$ass}, $clean_ali{$ass}, $contam_ali{$ass}, $added_seq{$ass}, $clean_seq{$ass}, $contam_seq{$ass}, $foreign_phyla{$ass}, $global_contam{$ass}, $contam_perc{$ass}, $unclassified_contam_perc{$ass}, $unknown_perc{$ass}, "NA", "NA")
            or die 'Couldn\'t connect to database: ' . $insert_checkm->errstr;
    }
    system("rm -rf $FTdir/TQMD_FT_prep_$ARGV_intrn_FT_task/banks/");
}

__END__

=head1 VERSION

version 0.2.1

=head1 USAGE

    $ tqmd_update.pl --tqmd-dir=<dir> --temp-dir=<dir> --config=<file> \
        --calc=<str> [optional arguments]

=head1 REQUIRED ARGUMENTS

=over

=item --tqmd-dir=<dir>

Full path to the place you want B<TQMD> to store the genomes and proteomes
(e.g., C</home/leonard/work/TQMD_DIR/>).

=for Euclid:
    dir.type: string

=item --temp-dir=<dir>

Full path to the place which will be used to store the temporary files required
by the sub-scripts to produce the data.

=for Euclid:
    dir.type: string

=item --calc=<str>

Type of calculation to be performed. This must be one string out of C<quast>,
C<annotation>, C<jellyfish>, C<rnammer>, C<fortytwo> and C<checkm>.

=for Euclid:
    str.type: /16scalc|FTprep|FTwrap|anncalc|annotation|cdhit|cdhitcalc|checkm|checkmcalc|fortytwo|jellycalc|jellyfish|quast|quastcalc|rnammer/
    str.type.error: <str> must be one of quast, annotation, jellyfish, rnammer, fortytwo or checkm (not str)

=item --config=<file>

Path to the INI file specifying B<TQMD> configuration. If this file is not
located in the same directory used to launch B<TQMD>, add the full path before
the name (e.g., C</home/leonard/work/TQMD_DIR/tqmd_config.ini>).

=for Euclid:
    file.type: readable

=back

=head1 OPTIONAL ARGUMENTS

=over

=item --config-42=<file>

Path to the INI file specifying B<42> configuration. If this file is not located
in the same directory used to launch B<TQMD>, add the full path before the name
(e.g., C</home/leonard/work/TQMD_DIR/tqmd_config_FT.ini>).

=for Euclid:
    file.type: string

=item --kmer-size=<n>

Size of the B<JELLYFISH> kmer [default: 12].

=for Euclid:
    n.type: +integer, n >= 11 && n <= 31
    n.default: 12

=item --kmer-canonical

When specified, canonical kmers are used (instead of strand-specific kmers).

=item --cdhit-threshold=<n>

B<CD-HIT> threshold determining when to cluster SSU rRNAs (16S) [default:
0.975].

=for Euclid:
    n.type: 0+number, n <= 1
    n.default: 0.975

=item --pack-size=<n>

Maximum number of genomes by sub-job of a job-array [default: 200].

=for Euclid:
    n.type: 0+integer
    n.default: 200

=item --max-array=<n>

Maximum number of sub-jobs/CPUs to run/use in parallel [default: 100].

=for Euclid:
    n.type: +integer, n <= 300
    n.default: 100

=item --threads=<n>

Number of threads/CPUs to be used by B<JELLYFISH>, B<QUAST> and B<CheckM>
[default: 1].

=for Euclid:
    n.type: +integer, n <=8
    n.default: 1

=item --single-node

When specified, B<TQMD> works on a single node (instead of the grid engine).

=item --intrn-gca-list=<file>

DO NOT USE. The name you gave to a file containing a subset of GCF you wish to
work with. If this option is not used, B<TQMD> uses every genome available. If
the file is not located in the same directory you are using to launch B<TQMD>,
add the full path before the name.

=for Euclid:
    file.type: str

=item --intrn-task=<n>

Tasknumber of quastcalc

=for Euclid:
    n.type: integer

=item --intrn-FT-task=<n>

FT task number

=for Euclid:
    n.type: 0+integer

=item --version

=item --usage

=item --help

=item --man

Print the usual program information

=back

=head1 AUTHORS

=over

=item Raphael R. LEONARD <rleonard@doct.uliege.be>

=item Denis BAURAIN <denis.baurain@uliege.be>

=back

=head1 LICENSE AND COPYRIGHT

This software is copyright (c) 2020 by University of Liege / Unit of Eukaryotic
Phylogenomics / Raphael R. LEONARD and Denis BAURAIN.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
