#!/usr/bin/env perl
# PODNAME: tqmd_cluster.pl
# ABSTRACT: Script for computing distances between genomes and clustering them.

use Modern::Perl '2011';
use autodie;

use Getopt::Euclid qw(:vars);
use Smart::Comments;

use Array::Split qw( split_by split_into );
use Config::IniFiles;
use Data::UUID;
use DBI;
use File::Basename;
use File::Find::Rule;
use File::Temp;
use List::AllUtils qw(shuffle uniq min max all any);
use List::Compare;
use IPC::System::Simple qw(system);
use Parallel::Batch;
use Path::Class qw(dir);
use Statistics::Data::Rank;
use Template;

use Bio::MUST::Core;
use aliased 'Bio::MUST::Core::Taxonomy';

use FindBin qw($Bin); use lib "$Bin/../lib";

use TQMD qw(get_genomic get_gcas_infos_dna get_gcas_infos_dna_custom get_new_gcas_file get_list_compare);


# path for the taxdump
my $taxdir = dir($ARGV_tqmd_dir, 'taxdir');
my $tax;

my $archives_path = dir($ARGV_tqmd_dir, 'archives', $ARGV_source)->stringify;
my $custom_path = dir($ARGV_tqmd_dir, 'archives', 'custom')->stringify;
### $archives_path
( my $tmpdiro = dir($ARGV_tqmd_dir, 'temp', $ARGV_temp_dir) )->mkpath();
my $tmpdir = $tmpdiro->stringify;
### $tmpdir
( my $rnammerdiro = dir($tmpdir, 'rnammer') )->mkpath();
my $rnammerdir = $rnammerdiro->stringify;
( my $annotationdiro = dir($tmpdir, 'annotation') )->mkpath();
my $annotationdir = $annotationdiro->stringify;
( my $jellyfishdiro = dir($tmpdir, 'jellyfish') )->mkpath();
my $jellyfishdir = $jellyfishdiro->stringify;
( my $quastdiro = dir($tmpdir, 'quast') )->mkpath();
my $quastdir = $quastdiro->stringify;

my $ini_cfg = new Config::IniFiles -file => "$ARGV_config";
my $dsn = $ini_cfg->val('Database', 'database');
### $dsn
my $dbn = $ini_cfg->val('Database', 'username');
### $dbn
my $dbp = $ini_cfg->val('Database', 'password');
### $dbp
my $dbh = DBI->connect($dsn, $dbn, $dbp)
    or die 'Couldn\'t connect to database: ' . DBI->errstr;

my $limit;

# setup ranking
# TODO: consider using Euclid to specify multiple metrics...
# .. instead of one large string that need to be split
$ARGV_ranking_formula =~ tr/ //d;   # clean-up whitespace
my @ranking_specs = split q{,}, $ARGV_ranking_formula;
### @ranking_specs

# check that all metrics are correctly spelled (and found in switch table)
my @ranking_metrics = map { tr/+-//dr } @ranking_specs;
### @ranking_metrics

my $quast_check = any { m/^quast\./xms } @ranking_metrics;
my $annot_check = any { m/^annot\./xms } @ranking_metrics;
my $FT_check    = any { m/^   42\./xms } @ranking_metrics;
my $CM_check    = any { m/^   cm\./xms } @ranking_metrics;

my $cg_opt = $ARGV_custom_genomes ? '--custom-genomes' : q{};
my $kc_opt = $ARGV_kmer_canonical ? '--kmer-canonical' : q{};
my $can_opt = ($ARGV_kmer_canonical && $ARGV_kmer_engine eq 'jellyfish') ? '-C'
                : ($ARGV_kmer_canonical && $ARGV_kmer_engine eq 'mash') ? q{}
                : ((! $ARGV_kmer_canonical) && $ARGV_kmer_engine eq 'mash') ? '-n'
                : q{};
### $can_opt
my $prior_opt = $ARGV_priority_gca_list ? "--priority-gca-list=$ARGV_priority_gca_list" : q{};

if ($ARGV_intrn_calc eq 'prep') {

    my $sel_nbr = 0;
    my @gcalist;
    ### check_gcalist
    if (defined $ARGV_gca_list) {
        open my $gcalistfile, '<', $ARGV_gca_list;
        while (my $line = <$gcalistfile>) {
            chomp $line;
            push @gcalist, $line;
        }
        $sel_nbr = @gcalist;
    }
    ### check gca dir
    if (defined $ARGV_intrn_gca_dir) {
        my @infiles = File::Find::Rule
        ->file()
        ->name( 'dualsize_*' )
        ->in($ARGV_intrn_gca_dir)
        ;
        foreach my $infile (@infiles) {
            open my $in, '<', $infile;
            while (my $line = <$in>) {
                chomp $line;
                push @gcalist, $line
            }
        }
        $sel_nbr = @gcalist;
    }
    ### check_unwanted_list
    my @unwanted_gcas;
    if (defined $ARGV_negative_gca_list) {
        open my $in, '<', $ARGV_negative_gca_list;
        while (my $line = <$in>) {
            chomp $line;
            push @unwanted_gcas, $line;
        }
    }
    ### $sel_nbr

    ### check_if_finished
    if ( ($ARGV_intrn_rr == $ARGV_max_round) || ($ARGV_intrn_last eq 'TRUE') || ( ((($ARGV_intrn_lrgn - $sel_nbr)/$ARGV_intrn_lrgn)*100) < $ARGV_delta_limit && ($ARGV_intrn_lrgn - $sel_nbr) >= 0 && $ARGV_intrn_rr > $ARGV_min_round) ) {
        my $endname = "$tmpdir/p$ARGV_pack_size\_l$ARGV{'--repr-threshold'}{'n_l'}\_t$ARGV_dist_threshold\_j$ARGV_kmer_size\_a$ARGV_max_array\_r$ARGV_max_round\_d$ARGV_delta_limit\_min$ARGV_min_round\_type$ARGV_dividing_scheme\_alg$ARGV_dist_metric\_egn$ARGV_kmer_engine.result";
        open (my $fh3, '>', $endname);
        foreach my $gca (@gcalist) {
            say $fh3 "$gca";
        }
        close $fh3;

        ### quastresfile
        my $quastresfile = "$tmpdir/p$ARGV_pack_size\_l$ARGV{'--repr-threshold'}{'n_l'}\_t$ARGV_dist_threshold\_j$ARGV_kmer_size\_a$ARGV_max_array\_r$ARGV_max_round\_d$ARGV_delta_limit\_min$ARGV_min_round\_type$ARGV_dividing_scheme\_alg$ARGV_dist_metric.quast";
        open (my $quastfile, '>', $quastresfile);
        say $quastfile join(';','GCA','ttllgt1kbp/ttllgt0bp','largest/ttllgt1kbp','largest/ttllgt0bp','n50/ttllgt0bp','n75/ttllgt0bp','100000-Nper100kbp');
        my $quastvals = $dbh->prepare('SELECT * FROM Quast');
        $quastvals->execute;
        my %check = map { $_ => 1 } @gcalist;
        while (my $result = $quastvals->fetchrow_hashref) {
            if (exists($check{$result->{gca}}) ) {
                my $first = $result->{ttllgt1kbp}/$result->{ttllgt0bp};
                my $second = $result->{largest}/$result->{ttllgt1kbp};
                my $third = $result->{largest}/$result->{ttllgt0bp};
                my $fourth = $result->{n50}/$result->{ttllgt0bp};
                my $fifth = $result->{n75}/$result->{ttllgt0bp};
                my $sixth = ( 100000 - ($result->{Nper100kbp}) );
                say $quastfile join(';',$result->{gca},$first,$second,$third,$fourth,$fifth,$sixth);
            }
        }
        close $quastfile;

        ### insert_results_db
        my $insert_taxfilters =
            $dbh->prepare_cached('INSERT INTO TaxFilters (name, tax_filter, description, date) VALUES (?,?,?,NOW())');
        my $tf_name = 'auto_update';
        my $tf = 'prokaryotes';
        my $tf_desc = 'refseq prokaryotes';
        $insert_taxfilters->execute($tf_name,$tf,$tf_desc)
            or die 'Couldn\'t execute statement: ' . $insert_taxfilters->errstr;
        my $last_tf_id = $insert_taxfilters->{mysql_insertid}
            or die 'Couldn\'t execute statement: ' . $insert_taxfilters->errstr;
        my $insert_runs =
            $dbh->prepare_cached('INSERT INTO Runs (clust_meth, data_type, data_transf, dist_type, target_number, date, tf_id, user_name) VALUES (?,?,?,?,?,NOW(),?,?)');
        my $r_cm = 'jellyfish';
        my $r_dat = 'dna';
        my $r_df = 'none';
        my $r_dit = $ARGV_dist_metric;
        $insert_runs->execute($r_cm,$r_dat,$r_df,$r_dit,$sel_nbr,$last_tf_id,'admin')
            or die 'Couldn\'t execute statement:' . $insert_runs->errstr;
        my $last_r_id = $insert_runs->{mysql_insertid}
            or die 'Couldn\'t execute statement: ' . $insert_runs->errstr;
        my @stored_clusters =
            @{ $dbh->selectcol_arrayref('SELECT cluster FROM Clusters') };
        my $c_id = 0;
        my $c_big_id;
        my $c_nbr = @stored_clusters;
        if ($c_nbr) {
            foreach my $cluster (@stored_clusters) {
                my ($name, $id) = split /_/xms, $cluster;
                if ($name eq 'auto_update') {
                    if ($id > $c_id) {
                        $c_id = $id;
                    }
                }
            }
        } else {
            $c_id++
        }
        my $c_cluster = join('_', 'auto_update', $c_id);
        my $insert_clusters =
            $dbh->prepare_cached('INSERT INTO Clusters (cluster, gca, is_master, run_id) VALUES (?,?,?,?)');
        foreach my $gca (@gcalist){
            $insert_clusters->execute($c_cluster, $gca, 1, $last_r_id)
                or die 'Couldn\'t execute statement: ' . $insert_clusters->errstr;
        }
        my @stored_coll =
            @{ $dbh->selectcol_arrayref('SELECT coll_autoid FROM Collections')};
        my $co_id = 0;
        foreach my $coid (@stored_coll) {
            if ($coid > $co_id) {
                $co_id = $coid;
            }
        }
        my $insert_pools =
            $dbh->prepare_cached('INSERT INTO Pools (coll_id, run_id) VALUES (?,?)');
        $insert_pools->execute($co_id, $last_r_id)
            or die 'Couldn\'t execute statement: ' . $insert_pools->errstr;

    } else {
        ### not_finished

        ### archive files
        my @archivefiles = @{ get_genomic($archives_path) };
        ### archive names
        my ($archives_rsync_gcas_ref, $archives_file_for_ref, $archives_path_for_ref) = get_gcas_infos_dna(@archivefiles);
        my @archives_rsync_gcas = @$archives_rsync_gcas_ref;

        my %custom_fullname_for;
        if ($ARGV_custom_genomes) {
            ### custom files
            my @customfiles = @{ get_genomic($custom_path) };
            ### custom names
            my ($custom_rsync_gcas_ref, $custom_file_for_ref, $custom_path_for_ref, $custom_fullname_for_ref) = get_gcas_infos_dna_custom(@customfiles);
            my @custom_rsync_gcas = @$custom_rsync_gcas_ref;
            %custom_fullname_for = %$custom_fullname_for_ref;
            push @archives_rsync_gcas, @custom_rsync_gcas;
        }
        ### archives: scalar @archives_rsync_gcas

        my @intersection;
        if ($ARGV_filter) {
            $tax //= Taxonomy->new_from_cache( tax_dir => $taxdir );
            my $filter = $tax->tax_filter($ARGV_filter);
            ### Active filter: $filter->all_specs
            FILTER:
            foreach my $gca (@archives_rsync_gcas) {
                my @taxonomy
                    = $gca =~ m/^GC(A|F)_/xms ? $tax->get_taxonomy($gca)
                    : $tax->fetch_lineage($custom_fullname_for{$gca} . '@1')
                ;
                next FILTER unless $filter->is_allowed( \@taxonomy );
                push @intersection, $gca;
            }
            ### filtered: scalar @archives_rsync_gcas
        } else {
            @intersection = @archives_rsync_gcas;
        }

        if (@unwanted_gcas) {
            @intersection = @{ get_list_compare(\@unwanted_gcas, \@intersection, 'complement') };
            ### unwanted: scalar @intersection
        }

        ### take only the listed gca (if list exist)
        if(@gcalist) {
            @intersection = @{ get_list_compare(\@intersection, \@gcalist, 'intersection') };
            ### gcalist: scalar @intersection
        }

        ### take only genomes with k-mers predicted (optionnal)
        if ($ARGV_kmer_engine eq 'jellyfish') {
            my $jelly_table = $ARGV_kmer_canonical ? "JellyfishC_$ARGV_kmer_size" : "Jellyfish_$ARGV_kmer_size";
            my @jellyfish_gcas = @{ $dbh->selectcol_arrayref("SELECT gca FROM $jelly_table") };
            @intersection = @{ get_list_compare(\@intersection, \@jellyfish_gcas, 'intersection') };
            ### k-mers: scalar @intersection
        }

        ### take only genomes with assembly quality information (optionnal)
        if ($quast_check) {
            my @quast_gcas = @{ $dbh->selectcol_arrayref('SELECT gca FROM Quast') };
            @intersection = @{ get_list_compare(\@intersection, \@quast_gcas, 'intersection') };
            ### quast: scalar @intersection
        }

        ### take only genomes with annotations information (optionnal)
        if ($annot_check) {
            my @ann_gcas = @{ $dbh->selectcol_arrayref('SELECT gca FROM Annotations') };
            @intersection = @{ get_list_compare(\@intersection, \@ann_gcas, 'intersection') };
            ### annotation: scalar @intersection
        }

        ### take only the genomes with predicted 16s (optionnal)
        if($ARGV_requires_SSU_rRNA){
            my @rna16s_gcas = @{ $dbh->selectcol_arrayref('SELECT gca FROM RNA16s') };
            @intersection = @{ get_list_compare(\@intersection, \@rna16s_gcas, 'intersection') };
            ### 16s: scalar @intersection
        }

        ### take only the genomes with FT results (optionnal)
        if($FT_check){
            # TO DO
            my @FT_gcas = @{ $dbh->selectcol_arrayref('SELECT gca FROM FT_result') };
            @intersection = @{ get_list_compare(\@intersection, \@FT_gcas, 'intersection') };
            ### FT: scalar @intersection
        }

        ### take only the genomes with CM results (optionnal)
        if ($CM_check) {
            my @CM_gcas = @{ $dbh->selectcol_arrayref('SELECT gca FROM CheckM') };
            @intersection = @{ get_list_compare(\@intersection, \@CM_gcas, 'intersection') };
            ### CM: scalar @intersection
        }

        ### take only the genomes from one or more collections
        if ($ARGV_collections) {
            my @names = split /,/xms, $ARGV_collections;
            my @collection_collids;
            foreach my $name (@names) {
                $name =~ s/^\s+|\s+$//g;
                my $query = $dbh->prepare('SELECT coll_autoid FROM Collections WHERE name = ?');
                $query->execute($name);
                @collection_collids = (@collection_collids, @{ $dbh->selectcol_arrayref($query) });
            }
            ### @collection_collids
            my @collection_gcf;
            foreach my $collid (@collection_collids) {
                my $query = "SELECT gca FROM Entries WHERE coll_id = $collid";
                @collection_gcf = (@collection_gcf, @{ $dbh->selectcol_arrayref($query) });
            }
            @intersection = @{ get_list_compare(\@intersection, \@collection_gcf, 'intersection') };
            ### Collections: scalar @intersection
        }

        ### stop check
        unless (@intersection) {
            die "The selection of genomes is empty! Did you run tqmd_update.pl for every metric of the ranking formula?";
        }

        my $last_lim = ($ARGV{'--repr-threshold'}{'n_l'}/100)*(100+$ARGV{'--repr-threshold'}{'n_p'});
        my $last = scalar(@intersection) <= $last_lim ? 'TRUE' : 'FALSE';

        # random mode (no need for taxonomy)
        my @arranged_gcf = shuffle @intersection;

        # taxonomic mode (requires tax object)
        unless ($ARGV_dividing_scheme eq 'random') {
            $tax //= Taxonomy->new_from_cache( tax_dir => $taxdir );
            my %tax_for;
            foreach my $gca (@intersection) {
                my @taxonomy =
                    $gca =~ m/^GC(A|F)_/xms ? $tax->get_taxonomy($gca)
                    : $tax->fetch_lineage($custom_fullname_for{$gca} . '@1')
                ;
                $tax_for{$gca} = join q{; }, @taxonomy;
            }
            @arranged_gcf = sort {
                $tax_for{$a} cmp $tax_for{$b} || $a cmp $b
            } keys %tax_for;
        }
        ### @arranged_gcf
        my $gca_nbrs = @arranged_gcf;

        my @sub_arrays = split_by(
            $last eq 'TRUE' ? $last_lim : $ARGV_pack_size,
            @arranged_gcf
        );
        ### @sub_arrays

        my $nbr_name = 1;
        my @nbr_array;
        foreach my $sub_array (@sub_arrays) {
            my @subarray = @{ $sub_array };
            open(my $fh, '>', "$tmpdir/distinfolist_$ARGV_intrn_rr\_$nbr_name");
            say $fh join("\n", @subarray);
            close $fh;
            push @nbr_array, "$tmpdir/distinfolist_$ARGV_intrn_rr\_$nbr_name";
            $nbr_name++;
        }
        $limit = @nbr_array;
        open (my $fh2, '>', "$tmpdir/distlist_$ARGV_intrn_rr");
        say $fh2 join("\n", @nbr_array);
        close $fh2;
        my $round = $ARGV_intrn_rr+1;
        ### @nbr_array

my $ug = Data::UUID->new;
my $uuid = $ug->create();
my $rdmstr = $ug->to_string( $uuid );

        if ($ARGV_single_node) {
            my @array4batch;
            my $list_number = 1;
            my $array_master = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_master} <<"EOT";
#!/bin/bash

cd $ARGV_tqmd_dir

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --pack-size=$ARGV_pack_size --repr-threshold=$ARGV{'--repr-threshold'}{'n_l'}x$ARGV{'--repr-threshold'}{'n_p'} --max-array=$ARGV_max_array --max-round=$ARGV_max_round --temp-dir=$ARGV_temp_dir --dist-threshold=$ARGV_dist_threshold --threads=$ARGV_threads --kmer-size=$ARGV_kmer_size --intrn-rr=$round --intrn-gca-dir=temp/$ARGV_temp_dir/distfold_$ARGV_intrn_rr --intrn-lrgn=$gca_nbrs --intrn-last=$last --min-round=$ARGV_min_round --dividing-scheme=$ARGV_dividing_scheme --delta-limit=$ARGV_delta_limit --dist-metric=$ARGV_dist_metric --clustering-mode=$ARGV_clustering_mode --kmer-engine=$ARGV_kmer_engine $kc_opt $prior_opt $cg_opt --source=$ARGV_source --ranking-formula=$ARGV_ranking_formula --single-node
EOT

close $array_master;
            foreach my $work_list (@nbr_array) {
                my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash

cd $ARGV_tqmd_dir

mkdir -p temp/$ARGV_temp_dir/distfold_$ARGV_intrn_rr
mkdir -p /tmp/$ARGV_temp_dir/

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --intrn-calc=calc --pack-size=$ARGV_pack_size --repr-threshold=$ARGV{'--repr-threshold'}{'n_l'}x$ARGV{'--repr-threshold'}{'n_p'} --max-array=$ARGV_max_array --max-round=$ARGV_max_round --temp-dir=$ARGV_temp_dir --dist-threshold=$ARGV_dist_threshold --threads=$ARGV_threads --gca-list=$work_list --kmer-size=$ARGV_kmer_size --intrn-rr=$ARGV_intrn_rr --min-round=$ARGV_min_round --dividing-scheme=$ARGV_dividing_scheme --delta-limit=$ARGV_delta_limit --dist-metric=$ARGV_dist_metric --clustering-mode=$ARGV_clustering_mode --kmer-engine=$ARGV_kmer_engine $kc_opt $prior_opt --source=$ARGV_source --ranking-formula=$ARGV_ranking_formula > temp/$ARGV_temp_dir/distfold_$ARGV_intrn_rr/dualsize_$ARGV_intrn_rr\_$list_number

EOT

close $array_sub;
                $list_number++;
                push @array4batch, $array_sub;
            }
            ### @array4batch
            my $maxprocs_PB = min($limit, $ARGV_max_array);
            my $calc_batch = Parallel::Batch->new( {
                maxprocs => $maxprocs_PB,
                jobs => \@array4batch,
                code => sub {
                        ### launch_array
                        my $sh_file_array = shift;
                        ### sh: $sh_file_array->filename
                        ### ct: `cat $sh_file_array`
                        system("sh $sh_file_array");
                        ### sh done
                    },
                progress_cb => {
                    done => sub {
                                ### launch_master
                                system("sh $array_master");
                                ### finish_master
                            }
                }
            } );
            ### launch_run
            $calc_batch->run();
            ### stop_run
            #~ my $master_batch = Parallel::Batch->new( {
                #~ maxprocs => $ARGV_max_array,
                #~ jobs => $array_master,
                #~ code => sub {
                        #~ my $sh_file_master = shift;
                        #~ system("sh $sh_file_master");
                    #~ }
            #~ } );
            #~ $master_batch->run();
        } else {
            my $array_sub = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_sub} <<"EOT";
#!/bin/bash
#\$ -S /bin/bash
#\$ -V
#\$ -cwd
#\$ -q *s.q
#\$ -N TRQEMADA_$rdmstr
#\$ -t 1-$limit
#\$ -tc $ARGV_max_array

cd $ARGV_tqmd_dir

mkdir -p temp/$ARGV_temp_dir/distfold_$ARGV_intrn_rr
mkdir -p /tmp/$ARGV_temp_dir/

parameter=`sed -n "\${SGE_TASK_ID} p" temp/$ARGV_temp_dir/distlist_$ARGV_intrn_rr`

$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --intrn-calc=calc --pack-size=$ARGV_pack_size --repr-threshold=$ARGV{'--repr-threshold'}{'n_l'}x$ARGV{'--repr-threshold'}{'n_p'} --max-array=$ARGV_max_array --max-round=$ARGV_max_round --temp-dir=$ARGV_temp_dir --dist-threshold=$ARGV_dist_threshold --threads=$ARGV_threads --gca-list=\${parameter} --kmer-size=$ARGV_kmer_size --intrn-rr=$ARGV_intrn_rr --min-round=$ARGV_min_round --dividing-scheme=$ARGV_dividing_scheme --delta-limit=$ARGV_delta_limit --dist-metric=$ARGV_dist_metric --clustering-mode=$ARGV_clustering_mode --kmer-engine=$ARGV_kmer_engine $kc_opt $prior_opt $cg_opt --source=$ARGV_source --ranking-formula=$ARGV_ranking_formula > temp/$ARGV_temp_dir/distfold_$ARGV_intrn_rr/dualsize_$ARGV_intrn_rr\_\${SGE_TASK_ID}

EOT

close $array_sub;

my $array_master = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.sh' );
print {$array_master} <<"EOT";
#!/bin/bash
#\$ -S /bin/bash
#\$ -V
#\$ -cwd
#\$ -q bignode.q
#\$ -N TRQEMADA-M_$rdmstr
cd $ARGV_tqmd_dir
$0 --config=$ARGV_config --tqmd-dir=$ARGV_tqmd_dir --pack-size=$ARGV_pack_size --repr-threshold=$ARGV{'--repr-threshold'}{'n_l'}x$ARGV{'--repr-threshold'}{'n_p'} --max-array=$ARGV_max_array --max-round=$ARGV_max_round --temp-dir=$ARGV_temp_dir --dist-threshold=$ARGV_dist_threshold --threads=$ARGV_threads --kmer-size=$ARGV_kmer_size --intrn-rr=$round --intrn-gca-dir=temp/$ARGV_temp_dir/distfold_$ARGV_intrn_rr --intrn-lrgn=$gca_nbrs --intrn-last=$last --min-round=$ARGV_min_round --dividing-scheme=$ARGV_dividing_scheme --delta-limit=$ARGV_delta_limit --dist-metric=$ARGV_dist_metric --clustering-mode=$ARGV_clustering_mode --kmer-engine=$ARGV_kmer_engine $kc_opt $prior_opt $cg_opt --source=$ARGV_source --ranking-formula=$ARGV_ranking_formula
EOT

close $array_master;

            system("qsub -pe snode $ARGV_threads $array_sub");
            system("qsub -hold_jid TRQEMADA_$rdmstr $array_master");
        }
    }

} elsif ($ARGV_intrn_calc eq 'calc') {

    ### find_path
    my @dnafiles = @{ get_genomic($archives_path) };
    ### check priority gca list
    my @gca_to_boost;
    if (defined $ARGV_priority_gca_list) {
        open my $gcalistfile, '<', $ARGV_priority_gca_list;
        while (my $line = <$gcalistfile>) {
            chomp $line;
            #~ next if /^$/;
            push @gca_to_boost, $line;
        }
    }
    ### filename
    my ($rsync_gcas_ref, $file_for_ref, $path_for_ref) = get_gcas_infos_dna(@dnafiles);
    my @rsync_gcas = @$rsync_gcas_ref;
    my %file_for = %$file_for_ref;
    my %path_for = %$path_for_ref;

    my %custom_fullname_for;
    if ($ARGV_custom_genomes) {
        ### custom files
        my @customfiles = @{ get_genomic($custom_path) };
        ### custom names
        my ($custom_rsync_gcas_ref, $custom_file_for_ref, $custom_path_for_ref, $custom_fullname_for_ref) = get_gcas_infos_dna_custom(@customfiles);
        my @custom_rsync_gcas = @$custom_rsync_gcas_ref;
        my %custom_file_for = %$custom_file_for_ref;
        my %custom_path_for = %$custom_path_for_ref;
        %custom_fullname_for = %$custom_fullname_for_ref;
        push @rsync_gcas, @custom_rsync_gcas;
        %file_for = (%file_for, %custom_file_for);
        %path_for = (%path_for, %custom_path_for);
    }

    # retrieve the list of gca from prep phase
    my ($gcalist_ref, $taskid) = get_new_gcas_file($ARGV_gca_list);
    my @gcalist = @$gcalist_ref;

    my $nbr_gca = @gcalist;

    # retrieve jellyfish info
    my $sthjell;
    my %jellyfish_ss;
    if ($ARGV_kmer_engine eq 'jellyfish') {
        my $jelly_table = $ARGV_kmer_canonical ? "JellyfishC_$ARGV_kmer_size" : "Jellyfish_$ARGV_kmer_size";
        $sthjell = $dbh->prepare("SELECT * FROM $jelly_table");
        $sthjell->execute;
        while (my $result = $sthjell->fetchrow_hashref) {
            $jellyfish_ss{$result->{gca}} = $result->{distids};
        }
    }

    # === RANKING PART

    # declare all possible hashrefs for ranking
    # Note: we have to do this early to compile the switch table just below
    my $quast_ref = {};
    my $annot_ref = {};
    my $ft_ref = {};
    my $cm_ref = {};

    # declare switch table for ranking
    my %metric_for = (

        # QUAST
        'quast.contigs.0.bp'      => sub { $quast_ref->{$_[0]}->{'ctg0bp'    } },
        'quast.contigs.1000.bp'   => sub { $quast_ref->{$_[0]}->{'ctg1kbp'   } },
        'quast.total.len.0.bp'    => sub { $quast_ref->{$_[0]}->{'ttllgt0bp' } },
        'quast.total.len.1000.bp' => sub { $quast_ref->{$_[0]}->{'ttllgt1kbp'} },
        'quast.contigs'           => sub { $quast_ref->{$_[0]}->{'ctg'       } },
        'quast.largest.contig'    => sub { $quast_ref->{$_[0]}->{'largest'   } },
        'quast.total.len'         => sub { $quast_ref->{$_[0]}->{'ttllgt'    } },
        'quast.gc.perc'           => sub { $quast_ref->{$_[0]}->{'gc'        } },
        'quast.n50'               => sub { $quast_ref->{$_[0]}->{'n50'       } },
        'quast.n75'               => sub { $quast_ref->{$_[0]}->{'n75'       } },
        'quast.l50'               => sub { $quast_ref->{$_[0]}->{'l50'       } },
        'quast.l75'               => sub { $quast_ref->{$_[0]}->{'l75'       } },
        'quast.N.per.100.kbp'     => sub { $quast_ref->{$_[0]}->{'Nper100kbp'} },

        # annotation
        'annot.certainty'         => sub { $annot_ref->{$_[0]}->{'certitude' } },
        'annot.completeness'      => sub { $annot_ref->{$_[0]}->{'completude'} },

        # Forty-Two
        # tested-genes,
        '42.added.ali'            => sub { $ft_ref->{$_[0]}->{'enrich-ali'} },    # note the name for clarity
        '42.clean.ali'            => sub { $ft_ref->{$_[0]}->{ 'clean-ali'} },
        '42.contam.ali'           => sub { $ft_ref->{$_[0]}->{'contam-ali'} },
        # completeness,             # TODO: include this?
        '42.added.seq'            => sub { $ft_ref->{$_[0]}->{ 'added_seq'} },
        '42.clean.seq'            => sub { $ft_ref->{$_[0]}->{ 'clean-seq'} },
        '42.contam.seq'           => sub { $ft_ref->{$_[0]}->{'contam-seq'} },
        # unclassified-contam-seq,  # TODO: include this?
        # unknown-seq,              # TODO: include this?
        '42.foreign.phyla'        => sub { $ft_ref->{$_[0]}->{           'foreign-phyla'} },
        # TODO: clarify the meaning of all these variants
        '42.contam.perc'          => sub { $ft_ref->{$_[0]}->{      'global-contam-perc'} },
        '42.class.contam.perc'    => sub { $ft_ref->{$_[0]}->{             'contam-perc'} },
        '42.unclass.contam.perc'  => sub { $ft_ref->{$_[0]}->{'unclassified-contam-perc'} },
        '42.unknown.perc'         => sub { $ft_ref->{$_[0]}->{             'unknow-perc'} },

        # CheckM
        'cm.completeness'         => sub { $cm_ref->{$_[0]}->{'Completeness'        } },
        'cm.contamination'        => sub { $cm_ref->{$_[0]}->{'Contamination'       } },
        'cm.strain.heterogeneity' => sub { $cm_ref->{$_[0]}->{'Strain_heterogeneity'} },

        # custom metrics
        'quast.largest.contig.ratio'
            => sub { $quast_ref->{$_[0]}->{'largest'} / $quast_ref->{$_[0]}->{'ttllgt1kbp'} },

        # pseudo-custom metrics that are not needed:
        # - 100000 - ($result->{Nper100kbp})        (simply use '-' when ranking)
        # - 100-($result->{'global-contam-perc'})
        # - 100-($result->{'Contamination'})
        # - 100-($result->{'Strain_heterogeneity'})
        # - (($result->{'enrich-ali'})/90)          (div by constant does not affect rank)
    );

    # defined in Euclid as default formula:
    # -quast.N.per.100.kbp, +quast.largest.contig.ratio, +annot.certainty, -42.global.contam.perc, +42.added.ali
    # TODO: check if +42.added.ali is not similar to relative completeness?


    die <<'EOT' unless all { exists $metric_for{$_} } @ranking_metrics;
Incorrect --ranking-formula: one or more metrics do not exist; please revise.
EOT

    # derive ranking order from metric signs
    # Note: we consider missing signs as '+' (this could be documented)
    my %order_for = map {
        m/^([+-])?(\S+)$/xms; defined $1 && $1 eq '-' ? ($2 => -1) : ($2 => +1)
    } @ranking_specs;
    ### %order_for

    # strategy taken from https://metacpan.org/pod/DBI#fetchall_hashref
    # $dbh->{FetchHashKeyName} = 'NAME_lc';
    # $sth = $dbh->prepare("SELECT FOO, BAR, ID, NAME, BAZ FROM TABLE");
    # $sth->execute;
    # $hash_ref = $sth->fetchall_hashref('id');
    # print "Name for id 42 is $hash_ref->{42}->{name}\n";

    # fill-in hashref from database tables
    # Note: we only fetch data needed by the ranking formula
    $quast_ref = $dbh->selectall_hashref('SELECT * FROM Quast',       'gca')
        if $quast_check;
    $annot_ref = $dbh->selectall_hashref('SELECT * FROM Annotations', 'gca')
        if $annot_check;
    $ft_ref    = $dbh->selectall_hashref('SELECT * FROM FT_result',   'gca')
        if $FT_check;
    $cm_ref    = $dbh->selectall_hashref('SELECT * FROM CheckM',      'gca')
        if $CM_check;

    # collect metrics and set their sign for each gca
    my %vals4ranking;
    for my $gca (@gcalist) {
        $vals4ranking{$gca} = [
            map { $order_for{$_} * $metric_for{$_}->($gca) } @ranking_metrics
        ];
    }

    ### %vals4ranking

    # ranking
    my $rank = Statistics::Data::Rank->new();
    my $ranks_href = $rank->ranks_between(data => \%vals4ranking);
    $rank->load(\%vals4ranking);
    $ranks_href = $rank->ranks_within();
    my $sor = $rank->sum_of_ranks_between();
    my %sor = %{ $sor };
    my @sor = keys %sor;
    my %vals_sl;
    foreach my $elem (@gcalist) {
        $vals_sl{$elem} = $sor{$elem};
    }
    #~ ### %vals_sl
    if (@gca_to_boost) {
        foreach my $elem (@gca_to_boost) {
            if ($vals_sl{$elem}) {
                $vals_sl{$elem} = (1000000 + $sor{$elem});
            }
        }
    }
    #~ ### %vals_sl
    my @sorted_gca;
    ### 1er sort
    for my $f (sort { $vals_sl{$b} <=> $vals_sl{$a} || $a cmp $b } keys %vals_sl) {
        push @sorted_gca, $f;
    }

    # triangular matrix
    my $dep_jellyfish = $ini_cfg->val('Dependencies', 'jellyfish_exe');
    my $dep_mash = $ini_cfg->val('Dependencies', 'mash_exe');
    my %groups;
    my $group_count = 0;
    GENOME:
    for( my $i = 0 ; $i < $nbr_gca ; $i++ ) {
        my $current_gca = $sorted_gca[$i];
        ### triangle current gca: $current_gca
        if (%groups) {
            for (my $g = 1; $g <= $group_count ; $g++ ) {
                ### triangle current group: $g
                my @group_gcas = @{ $groups{$g} };
                my $group_size = @group_gcas;
                if ($ARGV_clustering_mode eq 'loose') {
                    for (my $j = 0 ; $j < $group_size ; $j++ ) {
                        my $second_genome = $group_gcas[$j];
                        ### triangle compare gca: $second_genome
                        my $file1 = $file_for{$current_gca};
                        ### path to current file: $file1
                        my $file2 = $file_for{$second_genome};
                        ### path to second genome: $file2
                        system("gunzip -d -c $file1 > /tmp/$ARGV_temp_dir/$current_gca.fna");
                        system("gunzip -d -c $file2 > /tmp/$ARGV_temp_dir/$second_genome.fna");
                        my $union;
                        my $somme;
                        my $intersection;
                        my $pair_score;
                        if ($ARGV_kmer_engine eq 'mash') {
                            system("$dep_mash dist $can_opt -k $ARGV_kmer_size /tmp/$ARGV_temp_dir/$current_gca.fna /tmp/$ARGV_temp_dir/$second_genome.fna > /tmp/$ARGV_temp_dir/$current_gca-$second_genome.list");
                            $pair_score = `cut -f3 /tmp/$ARGV_temp_dir/$current_gca-$second_genome.list`;
                            system("rm -rf /tmp/$ARGV_temp_dir/$current_gca-$second_genome.list");
                        } else {
                            system("$dep_jellyfish count $can_opt -m $ARGV_kmer_size -s 100M -t $ARGV_threads -o /tmp/$ARGV_temp_dir/$current_gca-$second_genome /tmp/$ARGV_temp_dir/$current_gca.fna /tmp/$ARGV_temp_dir/$second_genome.fna");
                            $union = `$dep_jellyfish stats /tmp/$ARGV_temp_dir/$current_gca-$second_genome* | grep Distinct: | cut -f2 -d':' | sed -e 's/^[ \t]*//'`;
                            system("rm -rf /tmp/$ARGV_temp_dir/$current_gca-$second_genome*");
                            $somme = ($jellyfish_ss{$current_gca}+$jellyfish_ss{$second_genome});
                            $intersection = $somme - $union;
                            if ($ARGV_dist_metric eq 'IGF') {
                            my $min;
                            if($jellyfish_ss{$current_gca}>$jellyfish_ss{$second_genome}){
                                    $min = $jellyfish_ss{$second_genome};
                                }else{
                                    $min = $jellyfish_ss{$current_gca};
                                }
                                my $score = $intersection / $min;
                                $pair_score = (1-$score);
                            } else {
                                my $score = $intersection / $union;
                                $pair_score = (1-$score);
                            }
                        }
                        ### triangle compare cleaning
                        system("rm -rf /tmp/$ARGV_temp_dir/$current_gca.fna");
                        system("rm -rf /tmp/$ARGV_temp_dir/$second_genome.fna");
                        if ( $pair_score <= $ARGV_dist_threshold ) {
                            push @{ $groups{$g} }, $current_gca;
                            next GENOME;
                        }
                    }
                } else {
                    my $second_genome = $group_gcas[0];
                    ### triangle compare gca: $second_genome
                    my $file1 = $file_for{$current_gca};
                    ### path to current file: $file1
                    my $file2 = $file_for{$second_genome};
                    ### path to second genome: $file2
                    system("gunzip -d -c $file1 > /tmp/$ARGV_temp_dir/$current_gca.fna");
                    system("gunzip -d -c $file2 > /tmp/$ARGV_temp_dir/$second_genome.fna");
                    my $union;
                    my $somme;
                    my $intersection;
                    my $pair_score;
                    if ($ARGV_kmer_engine eq 'mash') {
                        system("$dep_mash dist $can_opt -k $ARGV_kmer_size /tmp/$ARGV_temp_dir/$current_gca.fna /tmp/$ARGV_temp_dir/$second_genome.fna > /tmp/$ARGV_temp_dir/$current_gca-$second_genome.list");
                        $pair_score = `cut -f3 /tmp/$ARGV_temp_dir/$current_gca-$second_genome.list`;
                        system("rm -rf /tmp/$ARGV_temp_dir/$current_gca-$second_genome.list");
                    } else {
                        system("$dep_jellyfish count $can_opt -m $ARGV_kmer_size -s 100M -t $ARGV_threads -o /tmp/$ARGV_temp_dir/$current_gca-$second_genome /tmp/$ARGV_temp_dir/$current_gca.fna /tmp/$ARGV_temp_dir/$second_genome.fna");
                        $union = `$dep_jellyfish stats /tmp/$ARGV_temp_dir/$current_gca-$second_genome* | grep Distinct: | cut -f2 -d':' | sed -e 's/^[ \t]*//'`;
                        system("rm -rf /tmp/$ARGV_temp_dir/$current_gca-$second_genome*");
                        $somme = ($jellyfish_ss{$current_gca}+$jellyfish_ss{$second_genome});
                        $intersection = $somme - $union;
                        if ($ARGV_dist_metric eq 'IGF') {
                        my $min;
                        if($jellyfish_ss{$current_gca}>$jellyfish_ss{$second_genome}){
                            $min = $jellyfish_ss{$second_genome};
                        }else{
                            $min = $jellyfish_ss{$current_gca};
                        }
                        my $score = $intersection / $min;
                        $pair_score = (1-$score);
                        } else {
                            my $score = $intersection / $union;
                            $pair_score = (1-$score);
                        }
                    }
                    ### triangle compare cleaning
                    system("rm -rf /tmp/$ARGV_temp_dir/$current_gca.fna");
                    system("rm -rf /tmp/$ARGV_temp_dir/$second_genome.fna");
                    if ( $pair_score <= $ARGV_dist_threshold ) {
                        push @{ $groups{$g} }, $current_gca;
                        next GENOME;
                    }
                }
            }
        }
        push @{ $groups{++$group_count} }, $current_gca;
        ### triangle new group
    }

    # select a representative for each group
    # TODO: simplify this (but I don't exactly get it)
    # TODO: e.g. use get_taxonomy_with_levels
    my @selection ;
    $tax //= Taxonomy->new_from_cache( tax_dir => $taxdir );
    my $id = $taskid;
    my %cluster_hash;
    open (my $fh4, '>', "$tmpdir/p$ARGV_pack_size\_l$ARGV{'--repr-threshold'}{'n_l'}\_t$ARGV_dist_threshold\_j$ARGV_kmer_size\_a$ARGV_max_array\_r$ARGV_max_round\_rr$ARGV_intrn_rr\_$id\_d$ARGV_delta_limit\_min$ARGV_min_round\_type$ARGV_dividing_scheme\_alg$ARGV_dist_metric.list");
    foreach my $group (sort { $a <=> $b } keys %groups) {
        my @S = @{ $groups{$group} };
        my %qrks;
        my @grp_phylum;
        my @grp_class;
        my @grp_order;
        my @grp_family;
        my @grp_genus;
        foreach my $elem (@S) {
            $qrks{$elem} = $vals_sl{$elem};
            if ($elem =~ m/^GC(A|F)_/xms) {
                push @grp_phylum, $tax->get_term_at_level($elem, 'phylum');
                push @grp_class, $tax->get_term_at_level($elem, 'class');
                push @grp_order, $tax->get_term_at_level($elem, 'order');
                push @grp_family, $tax->get_term_at_level($elem, 'family');
                push @grp_genus, $tax->get_term_at_level($elem, 'genus');
            } else {
                my $seq_id = $custom_fullname_for{$elem} . '@1';
                my $taxonid = $tax->get_taxonomy_from_seq_id($seq_id);
                push @grp_phylum, $tax->get_term_at_level($taxonid, 'phylum');
                push @grp_class, $tax->get_term_at_level($taxonid, 'class');
                push @grp_order, $tax->get_term_at_level($taxonid, 'order');
                push @grp_family, $tax->get_term_at_level($taxonid, 'family');
                push @grp_genus, $tax->get_term_at_level($taxonid, 'genus');
            }
        }
        my %counts_phylum;
        my %counts_class;
        my %counts_order;
        my %counts_family;
        my %counts_genus;
        ++$counts_phylum{$_} for @grp_phylum;
        ++$counts_class{$_} for @grp_class;
        ++$counts_order{$_} for @grp_order;
        ++$counts_family{$_} for @grp_family;
        ++$counts_genus{$_} for @grp_genus;
        my $uniques_phylum = keys(%counts_phylum);
        my $uniques_class = keys(%counts_class);
        my $uniques_order = keys(%counts_order);
        my $uniques_family = keys(%counts_family);
        my $uniques_genus = keys(%counts_genus);
        # TODO: introduce blanks after semicolons?
        say $fh4 "$group\t$uniques_phylum;$uniques_class;$uniques_order;$uniques_family;$uniques_genus";
        my @Ss;
        # TODO: no need for loop and push here: store the result of sort
        foreach my $f (sort { $qrks{$b} <=> $qrks{$a} || $a cmp $b } keys %qrks) {
            push @Ss, $f;
        }
        push @selection, $Ss[0];
        foreach my $g (@S) {
            $cluster_hash{$g} = $Ss[0];
        }
    }
    close $fh4;
    foreach my $gca (@selection) {
        say "$gca";
    }

    #~ open (my $fh5, '>', "$tmpdir/p$ARGV_pack_size\_l$ARGV{'--repr-threshold'}{'n_l'}\_t$ARGV_dist_threshold\_j$ARGV_kmer_size\_a$ARGV_max_array\_r$ARGV_max_round\_rr$ARGV_intrn_rr\_$id\_d$ARGV_delta_limit\_min$ARGV_min_round\_type$ARGV_dividing_scheme\_alg$ARGV_dist_metric.clusterlist");
    open (my $fh5, '>', "$tmpdir/Interm_$ARGV_intrn_rr\_$id.clusterlist");
    foreach my $gca (keys %cluster_hash) {
        say $fh5 "$gca\t$cluster_hash{$gca}";
    }
    close $fh5;
}

__END__

=head1 VERSION

version 0.2.1

=head1 USAGE

    $ tqmd_cluster.pl --tqmd-dir=<dir> --temp-dir=<dir> --config=<file> \
        [optional arguments]

=head1 REQUIRED ARGUMENTS

=over

=item --tqmd-dir=<dir>

Full path to the place you want B<TQMD> to store the genomes and proteomes
(e.g., C</home/leonard/work/TQMD_DIR/>).

=for Euclid:
    dir.type: string

=item --temp-dir=<dir>

Full path to the place which will be used to store the temporary files required
by the sub-scripts to produce the data.

=for Euclid:
    dir.type: string

=item --config=<file>

Path to the INI file specifying TQMD configuration. If this file is not located
in the same directory used to launch B<TQMD>, add the full path before the name
(e.g., C</home/leonard/work/TQMD_DIR/tqmd_config.ini>).

=for Euclid:
    file.type: readable

=back

=head1 OPTIONAL ARGUMENTS

=over

=item --source=<str>

Source database to use for clustering (either NCBI GenBank or NCBI RefSeq,
respectively designated as C<genbank> and C<refseq>) [default: refseq].

=for Euclid:
    str.type:       /genbank|refseq/
    str.type.error: <str> must be one of genbank or refseq (not str)
    str.default: "refseq"

=item --filter=<file>

Path to an IDL file specifying the taxonomic filter to be applied when
clustering genomes [default: none].

Note that this tax_filter can only remove genomes. It cannot include genomes
that have not yet been introduced in the database using the other B<TQMD>
scripts. See C<tqmd_download.pl> for the exact syntax of such a filter.

=for Euclid:
    file.type: readable

=item --gca-list=<file>

Path to a file containing a subset of GCA/GCF numbers you wish to work with
[default: none]. If this option is not used, B<TQMD> uses every genome
available. If the file is not located in the same directory you are using to
launch B<TQMD>, add the full path before the name.

=for Euclid:
    file.type: readable

=item --negative-gca-list=<file>

Path to a file containing a list of every GCA/GCF numbers you do NOT want to use
[default: none]. If the file is not located in the same directory you are using
to launch B<TQMD>, add the full path before the name.

=for Euclid:
    file.type: readable

=item --custom-genomes

When specified, B<TQMD> will also take custom genomes into consideration
[default: no].

=item --requires-SSU-rRNA

When specified, B<TQMD> will only take into consideration the genomes where at
least one SSU rRNA (16S) sequence has been predicted by the rnammer sub-script
of C<tqmd_update.pl> [default: no].

=item --collections=<str>

Name of one or more collection(s) you want to use. Collection names correspond
to the C<--coll-name> option of C<tqmd_download.pl>. Names have to be provided
between single quotes and, if several collection names are given, they must be
separated by a comma (C<,>).

=for Euclid:
    str.type: str

=item --dist-metric=<str>

Distance metric to use for the clustering (either Jaccard Index (C<JI>) or
Identical Genome Fraction (C<IGF>) [default: JI]. Note that these distances are
conceptually closer to similarity metrics (i.e., the larger the metric value the
more similar the two genomes). Thus, the distance effectively used by B<TQMD> is
obtained either as 1-JI or 1-IGF.

=for Euclid:
    str.type:       /JI|IGF/
    str.type.error: <str> must be one of JI or IGF (not str)
    str.default: "JI"

=item --dist-threshold=<n>

Minimum threshold below which to cluster genomes together [default:
0.6]. The bigger the number, the more aggressive B<TQMD> becomes.

=for Euclid:
    n.type: 0+number, n <= 1
    n.default: 0.6

=item --clustering-mode=<str>

Heuristic to use when clustering genomes (either C<strict> or C<loose>)
[default: loose]. In C<loose> mode, each genome is compared against every other
genome until finding one that is close enough, whereas in C<strict> mode,
genomes are only compared to the representatives of each cluster. The C<strict>
mode is faster and less agglomerative than the C<loose> mode.

=for Euclid:
    str.type:       /strict|loose/
    str.type.error: <str> must be one of strict or loose (not str)
    str.default: "loose"

=item --dividing-scheme=<str>

Type of partitioning B<TQMD> uses to allocate genomes to sub-jobs (either
C<taxonomic> or C<random>) [default: taxonomic]. If C<random> is chosen,
C<--max-round> should be increased from 10 to 20-30.

=for Euclid:
    str.type:       /taxonomic|random/
    str.type.error: <str> must be one of taxonomic or random (not str)
    str.default: "taxonomic"

=item --kmer-engine=<str>

Kmer counting program to use to compute distance metrics (either C<jellyfish> or
C<mash>) [default: jellyfish]. If C<mash> is chosen, C<--dist-metric> must be
set to C<JI> and C<--kmer-size> should be increased from 12 to 14-16.

=for Euclid:
    str.type:       /jellyfish|mash/
    str.type.error: <str> must be one of jellyfish or mash (not str)
    str.default: "jellyfish"

=item --kmer-size=<n>

Size of the B<JELLYFISH> or B<Mash> kmer used for pairwise genome comparisons
[default: 12]. When C<--kmer-engine> is set to C<jellyfish>, this option further
fetches the corresponding single-genome values stored in the database. In
contrast, C<mash> computations always happen live.

=for Euclid:
    n.type: +integer, n >= 11 && n <= 31
    n.default: 12

=item --kmer-canonical

When specified, canonical kmers are used (instead of strand-specific kmers).

=item --priority-gca-list=<file>

Path to a file containing a subset of GCA/GCF numbers you wish to give priority
in the ranking [default: none]. If the file is not located in the same directory
you are using to launch B<TQMD>, add the full path before the name.

=for Euclid:
    file.type: readable

=item --ranking-formula=<formula>

Assembly-related metrics to be taken into account when ranking genomes to select
the cluster representatives. Each metric can be considered either in ascending
(C<+>) or decending (C<->) order. For example, a contamination metric should be
prefixed by C<-> (the lower the better) whereas a completeness metric should be
prefixed by C<+> (the higher the better).

By default, B<TQMD> ranking is computed as:

    -quast.N.per.100.kbp                # assembly quality
    +quast.largest.contig.ratio         # assembly quality
    +annot.certainty                    # annotation richness
    -42.contam.perc                     # contamination level
    +42.added.ali                       # completeness level

All available metrics are:

    quast.contigs.0.bp
    quast.contigs.1000.bp
    quast.total.len.0.bp
    quast.total.len.1000.bp
    quast.contigs
    quast.largest.contig
    quast.total.len
    quast.gc.perc
    quast.n50
    quast.n75
    quast.l50
    quast.l75
    quast.N.per.100.kbp
    quast.largest.contig.ratio
    annot.certainty
    annot.completeness
    42.added.ali
    42.clean.ali
    42.contam.ali
    42.added.seq
    42.clean.seq
    42.contam.seq
    42.foreign.phyla
    42.contam.perc
    42.class.contam.perc
    42.unclass.contam.perc
    42.unknown.perc
    cm.completeness
    cm.contamination
    cm.strain.heterogeneity

=for Euclid:
    formula.type: str
    formula.default: '-quast.N.per.100.kbp, +quast.largest.contig.ratio, +annot.certainty, -42.contam.perc, +42.added.ali'

=item --repr-threshold=<n_l>x<n_p>

Target number and tolerance value (in percent) for the number of desired
representative genomes [default: 200x25]. The default value translates to "two
hundred genomes approximated to 25%, i.e., maximum 250 genomes". This is the
first of the three stop conditions controlling the iterations of B<TQMD>.

=for Euclid:
    n_l.type: 0+integer
    n_l.default: 200
    n_p.type: +integer <= 100
    n_p.default: 25

=item --delta-limit=<n>

Minimum level of dereplication (in percent) to achieve between two successive
iterations to continue [default: 1]. This is the second of the three stop
conditions controlling the iterations of B<TQMD>.

=for Euclid:
    n.type: +number
    n.default: 1

=item --min-round=<n>

Minimum number of rounds required [default: 1].

=for Euclid:
    n.type: +integer
    n.default: 1

=item --max-round=<n>

Maximum number of iterative rounds allowed [default: 10]. There is generally no
reason to change this value except when setting the option C<--dividing-scheme>
to C<random>. This is the third of the three stop conditions controlling the
iterations of B<TQMD>.

=for Euclid:
    n.type: +integer
    n.default: 10

=item --pack-size=<n>

Maximum number of genomes by sub-job of a job-array [default: 200].

=for Euclid:
    n.type: 0+integer
    n.default: 200

=item --max-array=<n>

Maximum number of sub-jobs/CPUs to run/use in parallel [default: 100].

=for Euclid:
    n.type: +integer, n <= 300
    n.default: 100

=item --threads=<n>

Number of threads/CPUs to be used by B<JELLYFISH> [default: 1].

=for Euclid:
    n.type: +integer, n <=8
    n.default: 1

=item --single-node

When specified, B<TQMD> works on a single node (instead of the grid engine).

=item --intrn-calc=<str>

Type of calculation to be performed; only C<prep> is available to the user.

=for Euclid:
    str.type:       /prep|calc/
    str.type.error: <str> must be one of prep or calc (not str)
    str.default: "prep"

=item --intrn-gca-dir=<dir>

Path to input directories containing GCA/GCF number list files.

=for Euclid:
    dir.type: string

=item --intrn-rr=<n>

DO NOT USE ! Current run round.

=for Euclid:
    n.type: 0+integer
    n.default: 0

=item --intrn-lrgn=<n>

DO NOT USE ! Number of GCA/GCF numbers in previous round.

=for Euclid:
    n.type: integer
    n.default: -1

=item --intrn-last=<str>

DO NOT USE ! Last round flag.

=for Euclid:
    str.type: str
    str.default: "FALSE"

=item --version

=item --usage

=item --help

=item --man

Print the usual program information

=back

=head1 AUTHORS

=over

=item Raphael R. LEONARD <rleonard@doct.uliege.be>

=item Denis BAURAIN <denis.baurain@uliege.be>

=back

=head1 LICENSE AND COPYRIGHT

This software is copyright (c) 2020 by University of Liege / Unit of Eukaryotic
Phylogenomics / Raphael R. LEONARD and Denis BAURAIN.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
