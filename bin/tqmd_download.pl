#!/usr/bin/env perl
# PODNAME: tqmd_download.pl
# ABSTRACT: Script for retrieving completely sequenced genomes from the NCBI.

use Modern::Perl '2011';
use autodie;

use Getopt::Euclid qw(:vars);
use Smart::Comments;

use Config::IniFiles;
use DBI;
use File::Basename;
use File::Find::Rule;
use File::Temp;
use List::Compare;
use LWP::Simple;
use Path::Class qw(dir file);

use Bio::MUST::Core;
use aliased 'Bio::MUST::Core::SeqId';
use aliased 'Bio::MUST::Core::Taxonomy';
use aliased 'Bio::MUST::Core::Taxonomy::Filter';

use FindBin qw($Bin); use lib "$Bin/../lib";

use TQMD qw(get_genomic get_gcas_infos_dna get_list_compare);


# path for the taxdump
my $taxdir = dir($ARGV_tqmd_dir, 'taxdir');
my $tax;

if ($ARGV_setup_taxdir) {
    ### setup taxdir
    my %args;
    $args{source} = 'ncbi';
    Taxonomy->setup_taxdir($taxdir, \%args);
    $tax = Taxonomy->new( tax_dir => $taxdir );
    $tax->update_cache;
}

$tax //= Taxonomy->new_from_cache( tax_dir => $taxdir );

# setup filter
my $filter
    = $ARGV_filter ? $tax->tax_filter($ARGV_filter)
    : Filter->new( tax => $tax, _specs => [ qw(+Archaea +Bacteria) ] )
;
### Active filter: $filter->all_specs

my %tax_for;
my %species_for;

my %gcf_names;
my %gcf_paths;

my $report = file($taxdir, sprintf ("assembly_summary_%s.txt", $ARGV_source) );
### $report->stringify

open my $rep, '<', $report;

LINE:
while (my $line = <$rep>) {
    next LINE if ($line =~ m/^\#/xms);

    chomp $line;
    my @fields = split /\t/xms, $line;

    # determine assembly taxonomy
    my $gcf = $fields[0];
    my @taxonomy = $tax->get_taxonomy($gcf);

    # optionally filter out assembly
    next LINE unless $filter->is_allowed( \@taxonomy );

    # store taxonomic details for later use
    $tax_for{$gcf} = \@taxonomy;
    my $species = $taxonomy[-1];
    $species_for{$gcf} = $species;

    # setup download and storage paths
    my (undef, $ftp) = split /:/xms, $fields[19];
    my @elements = split /\//xms, $ftp;
    my $name = $elements[-1];
    $gcf_names{$gcf} = $name;
    my @path_parts = @elements[-5..-1];
    my $path = join '/', @path_parts;
    $gcf_paths{$gcf} = $path;
}

my $for_rsync_list
    = File::Temp->new(UNLINK => 0, EXLOCK => 0, SUFFIX => '.list' );
my @download_list;
foreach my $gcf (keys %gcf_names) {
    my $gen = "$gcf_paths{$gcf}\/$gcf_names{$gcf}_genomic.fna.gz";
    my $prot = "$gcf_paths{$gcf}\/$gcf_names{$gcf}_protein.faa.gz";
    say $for_rsync_list "$gen" ;
    say $for_rsync_list "$prot" ;
}

close $for_rsync_list;

my $rsync_tt = <<'EOT';
rsync --recursive --verbose --copy-links --times --log-file=%s --relative \
    --ignore-existing --prune-empty-dirs --files-from=%s \
    rsync://ftp.ncbi.nlm.nih.gov/genomes/all/ \
    %s
EOT

#~ ### $rsync_tt

### rsync
( my $downdiro = dir($ARGV_tqmd_dir, 'archives', $ARGV_source) )->mkpath();
my $download_path = $downdiro->stringify;
### $download_path
system (sprintf $rsync_tt, 'download_log.txt', $for_rsync_list, $download_path);

### find_path
my @dnafiles = @{ get_genomic($download_path) };

### filename
my ($rsync_gcas_ref, $file_for_ref, $path_for_ref)
    = get_gcas_infos_dna(@dnafiles);
my @rsync_gcas = @$rsync_gcas_ref;
my %path_for = %$path_for_ref;

### check new
my $ini_cfg = new Config::IniFiles -file => "$ARGV_config";
my $dsn = $ini_cfg->val('Database', 'database');
my $dbn = $ini_cfg->val('Database', 'username');
my $dbp = $ini_cfg->val('Database', 'password');
my $dbh = DBI->connect($dsn, $dbn, $dbp)
    or die 'Couldn\'t connect to database: ' . DBI->errstr;
my @stored_gcas
    = @{ $dbh->selectcol_arrayref('SELECT gca FROM Assemblies') };

### comparaison
my $new_gcas_ref = get_list_compare(\@stored_gcas, \@rsync_gcas, 'complement');
my @new_gcas = @$new_gcas_ref;

### db_update
$dbh = DBI->connect($dsn, $dbn, $dbp)
    or die 'Couldn\'t connect to database: ' . DBI->errstr;
# Store the collection new entry
my $coll_name = $ARGV_coll_name;
my $coll_desc = "Collection downloaded from $ARGV_source";
my $insert_collections = $dbh->prepare_cached(
    'INSERT INTO Collections (name, description, user, date) VALUES (?,?,?,NOW())'
);
$insert_collections->execute($coll_name, $coll_desc, 'admin')
    or die 'Couldn\'t execute statement: ' . $insert_collections->errstr;
# retrieve the coll_autoid from the entry we've just created in Collections
my $last_coll_id = $insert_collections->{mysql_insertid}
    or die 'Couldn\'t execute statement: ' . $insert_collections->errstr;

# store the new Assets's entry
my $insert_assets
    = $dbh->prepare_cached('INSERT INTO Assets VALUES (?,?)');
$insert_assets->execute($last_coll_id, 'public')
    or die 'Couldn\'t execute statement: ' . $insert_assets->errstr;
# store informations for every gca in two different tables, Assemblies and Entries
my $insert_assemblies
    = $dbh->prepare_cached('INSERT INTO Assemblies VALUES (?,?,?,?)');
my $insert_entries = $dbh->prepare_cached('INSERT INTO Entries VALUES (?,?)');
my @stored_collid_entries
    = @{ $dbh->selectcol_arrayref('SELECT DISTINCT(coll_id) FROM Entries') };

foreach my $gca (@new_gcas) {
    $insert_assemblies->execute($gca, $species_for{$gca}, $path_for{$gca}, 'admin')
        or die 'Couldn\'t execute statement: ' . $insert_assemblies->errstr;
    $insert_entries->execute($gca, $last_coll_id)
        or die 'Couldn\'t execute statement: ' . $insert_entries->errstr;
}
my $insert_organisms
    = $dbh->prepare_cached('INSERT INTO Organisms VALUES (?,?)');
my $update_organisms
    = $dbh->prepare_cached('UPDATE Organisms SET lineage=? WHERE ?');
foreach my $gca (@new_gcas) {
    $insert_organisms->execute($gca, join( q{; }, @{ $tax_for{$gca} } ))
        or die 'Couldn\'t execute statement: ' . $insert_organisms->errstr;
}

__END__

=head1 VERSION

version 0.2.1

=head1 USAGE

    $ tqmd_download.pl --tqmd-dir=<dir> --config=<file> --coll-name=<str> \
        [optional arguments]

=head1 REQUIRED ARGUMENTS

=over

=item --tqmd-dir=<dir>

Full path to the place you want B<TQMD> to store the genomes and proteomes
(e.g., C</home/leonard/work/TQMD_DIR/>).

=for Euclid:
    dir.type: string

=item --coll-name=<str>

Name of the update which will be stored within the database. We advise you to
use the date in the following format for the name C<YYYY_MM_DD>.

=for Euclid:
    str.type: string

=item --config=<file>

Path to the INI file specifying TQMD configuration. If this file is not located
in the same directory used to launch B<TQMD>, add the full path before the name
(e.g., C</home/leonard/work/TQMD_DIR/tqmd_config.ini>).

=for Euclid:
    file.type: readable

=back

=head1 OPTIONAL ARGUMENTS

=over

=item --source=<str>

Source database from where to download genomes and proteomes (either NCBI
GenBank or NCBI RefSeq, respectively designated as C<genbank> and C<refseq>)
[default: refseq].

=for Euclid:
    str.type:       /genbank|refseq/
    str.type.error: <str> must be one of genbank or refseq (not str)
    str.default: "refseq"

=item --filter=<file>

Path to an IDL file specifying the taxonomic filter to be applied when
downloading genomes [default: +Archaea, +Bacteria].

In a tax_filter, wanted taxa are to be prefixed by a C<+> symbol, whereas
unwanted taxa are to be prefixed by a C<-> symbol. Wanted taxa are linked by
logical ORs while unwanted taxa are linked by logical ANDs. Unwanted taxa should
not include wanted taxa because the former ones take precedence over the latter
ones. In contrast the opposite helps fine-tuning the tax_filter.

An example IDL file follows:

    +Archaea
    +Bacteria
    -Proteobacteria
    -Firmicutes

=for Euclid:
    file.type: readable

=item --setup-taxdir

Force update the local mirror of the NCBI Taxonomy database.

=item --version

=item --usage

=item --help

=item --man

Print the usual program information

=back

=head1 AUTHORS

=over

=item Raphael R. LEONARD <rleonard@doct.uliege.be>

=item Denis BAURAIN <denis.baurain@uliege.be>

=back

=head1 LICENSE AND COPYRIGHT

This software is copyright (c) 2020 by University of Liege / Unit of Eukaryotic
Phylogenomics / Raphael R. LEONARD and Denis BAURAIN.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
